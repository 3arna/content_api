var _draggedObj = null;


class DragNdrop extends React.Component{
  
  constructor(){
    super();
    
      this.state = {
        dragOver : false
      };
      
      this.handleDragStart = this.handleDragStart.bind(this);
      this.handleDrop = this.handleDrop.bind(this);
      this.handleDragOver = this.handleDragOver.bind(this);
      this.handleDragLeave = this.handleDragLeave.bind(this);
    
  }
    
  handleDragStart(e){
    const { branch } = this.props;
    console.log('dragStart', branch, e, this);
    
    e.stopPropagation(); 
    e.dataTransfer.effectAllowed = 'move';

    this.startedDrag();
    
    _draggedObj = branch;
  }
  
  handleDrop(e){
    console.log('drop');
    if (e.stopPropagation) {
      e.stopPropagation(); // stops the browser from redirecting.
    }
    
    this.droppped(_draggedObj);
   
    return false;
  }
  
  handleDragOver(e){
    e.preventDefault();
    e.stopPropagation();
    if (!this.state.dragOver){
      this.setState({ dragOver : true });
    }
  }
  
  handleDragLeave(e){
    e.preventDefault();
    e.stopPropagation();
    if (this.state.dragOver){
      this.setState({ dragOver : false });
    }
  }
}

window.app.extensions.DragNdrop = DragNdrop;