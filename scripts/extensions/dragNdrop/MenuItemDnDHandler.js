
class MenuItemDnDHandler extends app.extensions.DragNdrop{
  
   constructor(data){
    super();
    this.store = window.app.store;
    this.data = data;
  }
  
  startedDrag(){
    const { onDragging, selected } = this.props;
    if (selected)
      onDragging();
  }
  
  droppped(draggedObj){
    const { branch, onDragging, selected } = this.props;

    // Checks if it's not dropped on hisself
    console.log('numete', draggedObj._id, branch._id, draggedObj.parent, branch.parent);
   
    if (draggedObj._id !== branch._id){
      if (branch.type === 'field'){
         console.log(draggedObj.parent, branch.parent);
        if (draggedObj.parent !== branch.parent){
          // Imesti i fieldo tevo sectiona !!!
         
          // !!!! NERA sequence, tai praleidziama (butu imetamas uz fieldo virs kurio laikomas elementas)
          // dabar metamas i eiles gala
          
          //var parentChildren = tree[dropZone.parent].children;
          // var index = parentChildren.indexOf(dropZone._id);
      
          // var wrapped = _(parentChildren).splice(index, 0, draggedObj._id);
          // wrapped = wrapped.commit();
          // -----------------------------------------------------------------//
       
        
          this.store.changeParent(draggedObj, branch.parent);
        } else {
          onDragging();
          // !!!!! Praleista, nes DB  neturi sequence !!!! 
          // // Sukeisti elementus vietomis to pacio section vaiku masyve
          // var children = tree[draggedObj.parent].children;
          // children = _.without(children, draggedObj._id);
          
          // var index = children.indexOf(dropZone._id);
          // var wrapped = _(children).splice(index, 0, draggedObj._id);
          // wrapped = wrapped.commit();
        // tree[draggedObj.parent].children = children;
        
        }
      } else {
        this.store.changeParent(draggedObj, branch._id);
      }
    } else {
      onDragging();
    }
     this.setState({ dragOver: false });
  }
  
  render(){
    const { children, branch, onDragging, selected } = this.props;
    return (
        <li 
            draggable={branch.draggable}
            onDragStart={(e) => this.handleDragStart(e)}  
            onDrop={this.handleDrop} 
            onDragOver={this.handleDragOver}
            onDragLeave={this.handleDragLeave}
            
            className={classNames({
              'list-none': true, 
              'brd-white brd-dash': this.state.dragOver, 
              'mrg-tb-2px': !this.state.dragOver 
            })}>
          {children}
        </li>
    );
  }
}

window.app.extensions.MenuItemDnDHandler = MenuItemDnDHandler;