var DRAGGED_OBJECT = null;

var App = React.createClass({

  mixins: [Reflux.connect(app.store), Reflux.connect(app.entriesStore),  Reflux.connect(app.mainStore)],
  
  getInitialState: function(){
    this.store = window.app.store;  
    this.entriesStore = window.app.entriesStore;
    this.mainStore = window.app.mainStore;
    this.mainActions = window.app.mainActions;
  },
  
  render: function() {
    
   // console.log('RENDERING', {'state': this.state});
    
    const { Registration, Login, NotFound, Loading, Dashboard } = window.app.pages;
    const { state, props } = this;
    const { user } = this.mainStore.data;
    
    switch(props.page){
      case '':
      case 'login':
        return <Login isLoggedIn={!!user.authTokken} />
      case 'register':
        return <Registration isLoggedIn={!!user.authTokken} />
      case 'dashboard':
        return !user.authTokken ? !window.app.mainActions.redirectTo('') : <Dashboard />;
      case 'app':
        return !user.authTokken ? !window.app.mainActions.redirectTo('') : 
          !state.loaded 
            ? <Loading/>
            : <div>
                <app.components.Tree {...state} />
                <app.components.Editor {...state} />
              </div>
      default:
        return <NotFound page={props.page}/>
    }
  },
  
});

const renderRoute = () => {
  
  const location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');
  window.app.mainActions.goTo(location);
  
  ReactDOM.render(
    <App location={ location } page={ _.first(location) } />, 
    document.getElementById('app')
  );
};

window.addEventListener('hashchange', renderRoute, false);

renderRoute();

/*ReactDOM.render(
  <App />,
  document.getElementById('app')
);*/



