const renderRoute = () => {
  const location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');
  ReactDOM.render(
    <App 
      location={ location } 
      routePath={_.get(location, 0)} 
      routeParams={_.get(location, 1)} url="/api/content/tree"/>, document.getElementById('app'));
};

window.addEventListener('hashchange', renderRoute, false);

renderRoute();