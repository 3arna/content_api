class DropDownMenu extends React.Component{
  
  constructor(){
    super();
    this.store = window.app.store;
    this.actions = window.app.actions;
    this.entriesStore = window.app.entriesStore;
    this.entryActions = window.app.entryActions;
    this.state = {
      confirmMenuOn: null,
      selectedElementId: null
    };
    this.deleteElement = this.deleteElement.bind(this);
  }
  // +/- sutvarkyta
  addNewSection(e){
	    const { branchId, treeTypes } = this.props;
      window.app.mainActions.addInputField(treeTypes[0], branchId, 'section');
	}
	
	// +/- sutvarkyta
	addNewField(e){
	    const { branchId, treeTypes } = this.props;
      window.app.mainActions.addInputField(treeTypes[0], branchId, 'field');
	}
	
	// Reikes perdaryti, kai perkursiu entriesTree
	addNewEntity(e){
	 
	  const { branchId, treeTypes, entryId } = this.props;
	  
    window.app.mainActions.addInputField(treeTypes[1], branchId, 'entity', entryId);
	 // const { entryId, branch  } = this.props;
	 // this.entryActions.openInputField(branch._id, entryId, 'entity');
	}
	
	// +- done
	addNewEntry(e){
    const { branchId, entryId  } = this.props;
    console.log(branchId,  entryId);
  // this.entryActions.saveEntry({ parent: entryId, entry: null});
    this.entryActions.saveEntry({ parent: branchId, entry: entryId});
	}
	
	handleConfirm(e){
	  const { branchId } = this.props;
	  this.setState({ confirmMenuOn: true, selectedElementId: branchId });
	}
	
	deleteElement(confirmed){
	  const { currentTree, treeTypes } = this.props;
	  if (confirmed) {
	    {treeTypes[1] === currentTree && this.entryActions.deleteElement(this.state.selectedElementId, true)}
	    {treeTypes[0] === currentTree && this.actions.deleteElement(this.state.selectedElementId, true)}
	   // this.store.deleteElement(this.state.selectedElementId, true);
	  }
	  this.setState({ confirmMenuOn: false, selectedElementId: null});
	}
	
	get itemList(){
	  switch(this.props.branchType){
      case 'app':
        return [
          {text: 'Add section', onClick: this.addNewSection},
          //{text: 'Add field', onClick: this.addNewField}
        ];
      case 'section':
        return [
          {text: 'Add section', onClick: this.addNewSection},
          {text: 'Add field', onClick: this.addNewField},
          //{text: 'Edit this section'},
          {text: 'Delete this section', onClick: this.handleConfirm, isDangerous: true}
        ];
      case 'field':
        return [
          //{text: 'Edit this field'},
          {text: 'Delete this field', onClick: this.handleConfirm, isDangerous: true}
        ];
      default:
        return null;
    }
	}
	
	doNothing(e){
	  console.log('just smile');
	}
	
	get entryMenuItems(){
	  switch(this.props.branchType){
	    case 'app':
	    case 'section':   
        return [
          {text: 'Add new entry', onClick: this.addNewEntry//this.addNewSection
          },
      
        ];
      case 'field':
        return [
          {text: 'Add new entity', onClick: this.addNewEntity//this.addNewSection
            
          },
        
        ];
      case 'entry':
        return [
          {text: 'Delete this entry', onClick: this.handleConfirm //this.deleteEntry
          , isDangerous: true},
        ];
      case 'entity':
        return [
          {text: 'Delete this element', onClick: this.handleConfirm //this.handleConfirm
          , 
          isDangerous: true}
        ];
      default:
        return null;
    }
	}
	
	_getItemClassNames(item){ 
	  return classNames({
      'pdg-tb-2px pdg-rl-10px fnt-sm brd-rad-2px cursor-pointer hover-clr-white list-none': true,
      'clr-red hover-bg-red': item.isDangerous,
      'hover-bg-greenl1 trans': !item.isDangerous
    })
	}
   
  render(){
    const { active, pos, branchId, currentTree, treeTypes } = this.props;
    if(this.state.confirmMenuOn){
      return <app.components.ConfirmMenu confirmed={this.deleteElement}/>;
    }   
  console.log(this.props);
    if(!active){
      return false;
    }
    
    if(treeTypes[0] === currentTree){
      return this.itemList && 
        <div 
          className="pos-absolute bg-white w-150px brd-rad-2px pdg-2px clr-black pdg-0 mrg-0 boxshadow list-none fadeIn"
          style={{top: pos.y + 'px', left: pos.x + 'px'}}>
            
            {this.itemList.map((item) => 
              <li 
                key={item.text} 
                className={this._getItemClassNames(item)} 
                onClick={item.onClick && item.onClick.bind(this)}>
                  {item.text}
              </li>
            )}
            
        </div>
    }
      
    if (treeTypes[1] === currentTree){
      return this.entryMenuItems && 
        <div 
          className="pos-absolute bg-white w-150px brd-rad-2px pdg-2px clr-black pdg-0 mrg-0 boxshadow list-none fadeIn"
          style={{top: pos.y + 'px', left: pos.x + 'px'}}>
            { this.entryMenuItems.map((item) => 
              <li 
                key={item.text} 
                className={this._getItemClassNames(item)} 
                onClick={item.onClick && item.onClick.bind(this)}>
                  {item.text}
              </li>
            )}
        </div>
    }
  }
}

window.app.components.DropDownMenu = DropDownMenu;

