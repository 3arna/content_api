var selectedElementId = null;

class Section extends React.Component{
  
  constructor(){
    super();
  
    this.store = window.app.store;
    this.actions = window.app.actions;
    
    this.state={
      selected: false,
      status: 'edit'
    };
    
    this.handleClick = this.handleClick.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
  }
  
  componentWillMount(){
    const { branch } = this.props;
    
    this.state.selected = branch.selected;
    this.state.status = branch.status;
  }
  
  get iconClassNames(){
    return classNames({
      'fa fa-fw': true,
      'fa-angle-down': this.state.selected,
      'fa-angle-right': !this.state.selected
    });
  }
  
  _getSectionClassNames(isSelected, type){
    return classNames({
      'pdg-tb-3px fnt-sm list-none brd-0': true,
      'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none': true,
      'clr-white brd-black bg-none clr-black': type === 'app',
      'bg-black clr-white': type === 'section',
      'bg-none': type === 'field',
      'brd-rad-2px': ['field', 'section', 'app', 'entity'].indexOf(type) !== -1 || isSelected,
      'hover-bg-greenl3 hover-clr-white': ['field', 'section', 'app', 'entry'].indexOf(type) !== -1 && !isSelected,
      'bg-greenl1 clr-white': ['field', 'section', 'app', 'entry'].indexOf(type) !== -1 && isSelected,
      'brd-l-black bg-none': type === 'entry',
      'bg-none brd-b-greyd3 clr-greyd3 brd-dash': type === 'entity' && !isSelected,
      'bg-none brd-b-greenl2 clr-greenl2 brd-dash': type === 'entity' && isSelected,
    });
  }
  
  _getWrapperClassNames(isSelected){
    return classNames({
      'brd-r-green': isSelected,
      'cursor-pointer': !isSelected,
    });
  }

  // Toggle
  handleClick(){
    this.setState({ selected: !this.state.selected });
  }
  
  handleSelect(branch, currentApp, currentTree) {
    if (selectedElementId !== branch._id){
      selectedElementId = branch._id;
      window.location.hash = `app/${currentApp}/${currentTree}/${branch._id}`;
      
    }
  }
  
  handleChangeStatus(e){
     const { nodesTree } = this.props;
     nodesTree[selectedElementId].status = (nodesTree[selectedElementId].status === 'show' ? 'edit' : 'show');
     this.store.trigger(this.store.data);
  }
  
  render(){
    const {nodesTree, branch, contextMenu, entryId, currentApp, currentTree, currentSection} = this.props;
    
  
    const isSelected = branch._id === currentSection;
    
    //console.log('FROM SECTION', this.props);
    
    return(
      <app.extensions.MenuItemDnDHandler onDragging={!!branch.draggable && this.handleClick}
        key={branch._id} 
        branch = {branch}
        selected = {this.state.selected}
        >
          <div onClick={(e) => this.handleSelect(branch, currentApp, currentTree)} className={this._getWrapperClassNames(isSelected)}>
            <button 
              onContextMenu ={(e) => contextMenu(e, e.branch = branch, e.entryId = entryId)}  
              className={this._getSectionClassNames(isSelected, branch.type)} >
                {branch.type !== 'field' && !!branch.children && !!Object.keys(branch.children).length &&
                  <i className={this.iconClassNames} onClick={this.handleClick} />}
                {false && branch.type === 'entry' && <i className="fa fa-file-o fa-fw mrg-r-5px"/>}
                <span onDoubleClick={this.handleChangeStatus}>
                  {branch.value}
                </span>
            </button>
          </div>
          
          {!!branch.children && !!Object.keys(branch.children).length && this.state.selected &&
            <ul className="mrg-0 pdg-l-15px" branchType={branch.type}>
            
              {Object.keys(branch.children).sort().map((childBranchId) => 
                nodesTree[branch.children[childBranchId]].type !== 'entry' && (
               !!nodesTree[branch.children[childBranchId]]  && nodesTree[branch.children[childBranchId]].status !== 'edit'
                ? !!nodesTree[branch.children[childBranchId]] &&
                    <app.components.Section 
                      currentSection={currentSection}
                      currentApp={currentApp}
                      currentTree={currentTree}
                      key={ branch.children[childBranchId] } 
                      nodesTree={ nodesTree } 
                      branch={ nodesTree[branch.children[childBranchId]] } 
                      contextMenu={ contextMenu }
                      selectedElementId={ selectedElementId }/>
                : 
                !!nodesTree[branch.children[childBranchId]] &&
                    <app.components.EditableSection
                      currentSection={currentSection}
                      currentApp={currentApp}
                      currentTree={currentTree}
                      key={ branch.children[childBranchId] } 
                      nodesTree={ nodesTree } 
                      branch={ nodesTree[branch.children[childBranchId]] } 
                      contextMenu={ contextMenu }
                      selectedElementId={ selectedElementId }/>)
                )
              }

            </ul>
          }
      </app.extensions.MenuItemDnDHandler>
    );
  }
}

window.app.components.Section = Section;