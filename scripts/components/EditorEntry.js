class EditorEntry extends React.Component{
    
    constructor(){
        super();    
       
    this.entriesStore = window.app.entriesStore;
        this.entryActions = window.app.entryActions;
        this.store = window.app.store;
        this.state = {
          onlyLabel: false,
          selEntry: null,
          newValue: null,  
          value: '',
          rows: 1
        };
        this.addNewEntry = this.addNewEntry.bind(this);
        this.addNewEntity = this.addNewEntity.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
    }
    
    componentWillMount(){
    }
    
    _getToggleIconClassNames(branch){
      const { entryId } = this.props;
      return classNames({
        'fa fa-lg bg-white boxshadow brd-rad-50 clr-blackl3 valign-bot pos-absolute left-0 top-0': branch.type === 'entry',
        'fa-chevron-circle-down': branch.type === 'entry' && branch._selected[entryId],
        'fa-chevron-circle-right': branch.type === 'entry' && !branch._selected[entryId],
        'fa fa-chevron-down fnt-sm clr-greyd3 dsp-block': branch.type === 'section' && !!branch._selected[entryId],
        'fa fa-chevron-right fnt-sm clr-greyd3 dsp-block': branch.type === 'section' && !branch._selected[entryId]
      });
    }
    
      // Toggle
    handleToggle(branch) {
      
      const { entryId } = this.props;
      
      branch._selected[entryId] = !branch._selected[entryId];
      this.entriesStore.trigger(this.entriesStore.data);
    }
    
    _getSectionClassNames(isSelected, type){
      return classNames({
        'list-none brd-0 brd-rad-2px bg-none pdg-tb-10px pdg-rl-20px': true,
        'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none hover-clr-green': true,
        'clr-green': isSelected
      });
    }
    
    setValue(newValue){
        this.setState({newValue: newValue});
    }
    
    //jeigu simboliu skaicius mazeja, tada atimamas stulpelis.
    
    handleLayout(e){
      //////console.log(e.target.rows, e.target.scrollHeight, e.target.scrollWidth);
      // max 103 min 36
      if (e.target.scrollTop > 0){
        e.target.rows = e.target.rows + 1;
      //  this.setState({rows: e.target.rows});
     //   this.handleLayout(e);
        
        this.forceUpdate();
        this.handleLayout(e);
      }
      
      
    
    }
    
     handleOnChange(e){
      // const { value } = this.state;
      // if (e.target.value.length < value.length){
      //   e.target.rows -=1;
       
      // this.handleLayout(e);
         
      // }
       //////console.log('pries: ', e.target.rows , e.target.scrollTop);
       //e.target.rows -=1;
    
    // this.setState({ value: e.target.value, rows: e.target.rows });
       //////console.log('pp: ', e.target.rows, e.target.scrollTop);
    //   if (e.target.rows >= 1)
    //   this.setState({rows: e.target.rows});
     }
     
    deleteElement(confirmed){
  	  const { currentTree, treeTypes } = this.props;
  	  const { selEntry } = this.state;
  	  if (confirmed) {
  	    {treeTypes[1] === currentTree && this.entryActions.deleteElement(selEntry._id, true)}
  	   // {treeTypes[0] === currentTree && this.actions.deleteElement(this.state.selectedElementId, true)}
  	   // this.store.deleteElement(this.state.selectedElementId, true);
  	  }
  	  this.setState({ confirmMenuOn: false, selEntry: null});
  	}
     
    handleConfirm(e){
	    const { branch2 } = this.props;
	    this.setState({ confirmMenuOn: true, selEntry: branch2 });
	  }
	  
	  addNewEntry(branch){
	    const { entryId  } = this.props;
	    
      this.entryActions.saveEntry({ parent: branch._id, entry: entryId});
	  }
	  
	  addNewEntity(branch){
	    const { treeTypes, entryId } = this.props;
	    console.log(branch);
      window.app.mainActions.addInputField(treeTypes[1], branch._id, 'entity', entryId);
	  }
    
    render(){
      
      const { selected } = this.state;

      const {  
      entriesTree, 
      entryId,
      branch2,
      branch,
      treeTypes,
      currentTree
    } = this.props;

    if(this.state.confirmMenuOn){
      return <app.components.ConfirmMenu confirmed={this.deleteElement.bind(this)}/>;
    }
    
    return <main className="mrg-tb-20px pos-relative pdg-5px w-max-800px mrg-auto">
      <div className="w-100 fix-bb bg-greyl2 boxshadow">
        
        <div className="brd-t-grey">
          <i className="fa fa-times-circle fa-lg bg-white boxshadow brd-rad-50 clr-redl2 valign-bot pos-absolute right-0 top-0" 
            onClick={this.handleConfirm.bind(this)}/>
          <i className={this._getToggleIconClassNames(branch2)} 
            onClick={e => this.handleToggle(branch2)}/>
        </div>
        
        {!!entriesTree[branch2._id]._selected[entryId] &&
          <div className="pdg-20px">
            
              {Object.keys(branch2.children).map(key => 
                !!_.startsWith(key, 'field') &&
                <div key={key} className="tb w-100">
                  <label className="td brd-0 col-3 valign-top pdg-tb-10px">
                      {entriesTree[branch2.children[key]].value}:
                  </label>
                  
                  <div className="pos-relative td w-100">
                   {!!branch2.entries[branch2.children[key]] && Object.keys(branch2.entries[branch2.children[key]]).sort().map(id =>
                        <app.components.EditorEntityView 
                          key={id}
                          labelId = {branch2.entries[branch2.children[key]][id]}
                          entriesTree={entriesTree}
                          entryId={entryId}
                          data={entriesTree[branch2.entries[branch2.children[key]][id]].value}
                          branch={entriesTree[branch2.entries[branch2.children[key]][id]]}
                        />
                    )}
                    <span 
                      onClick={e => this.addNewEntity(entriesTree[branch2.children[key]])} 
                      className="dsp-inline pdg-rl-10px pdg-tb-5px bg-greyl1 brd-grey mrg-5px clr-blackl3 boxshadow">
                        <i className="fa fa-plus clr-blackl3 dsp-block"/>
                    </span>
                  </div>
                </div>
                )
              }
                
            {Object.keys(branch2.children).map(key => 
                !!_.startsWith(key, 'section') &&
             <main key={key} className="pdg-t-20px">
                
                <div className="tb w-100">
        
                  {!!branch2.entries[branch2.children[key]] && !!Object.keys(branch2.entries[branch2.children[key]]).length &&
                  <div className="td">
                  {console.log(branch2.children[key], entriesTree[branch2.children[key]], entryId)}
                    <i onClick={e => this.handleToggle(entriesTree[branch2.children[key]])} 
                    className={this._getToggleIconClassNames(entriesTree[branch2.children[key]])}/>
                  </div>
                  }
                  
                  <div className="td pdg-rl-5px">
                    {entriesTree[branch2.children[key]].value}
                  </div>
                  <div className="td w-100">
                    <div className="brd-b-grey"/>
                  </div>
                  <div className="td">
                    <i className="fa fa-plus pdg-tb-10px pdg-l-10px clr-blackl3" 
                      onClick={e => this.addNewEntry(entriesTree[branch2.children[key]])}/>
                  </div>
                </div>
                
                {false && <a className="dsp-inline" >
                  <span className="bg-blackl3 dsp-inline clr-white pdg-tb-10px pdg-rl-20px">
                   { false && <i className={this.iconClassNames} 
                    onClick = {e => this.handleToggle('entry')} /> }
                    {entriesTree[branch2.children[key]].value}</span>
                  <i className="fa fa-plus pdg-tb-10px pdg-rl-20px" onClick={e => this.addNewEntry(entriesTree[branch2.children[key]])}/>
                </a>}
                
                {!!entriesTree[branch2.children[key]]._selected[entryId] && !!branch2.entries[branch2.children[key]] && 
                  Object.keys(branch2.entries[branch2.children[key]]).sort().map(id =>
                
                 !!entriesTree[branch2.entries[branch2.children[key]][id]] 
                 &&
                  <app.components.EditorEntry
                    key = {id}
                    currentTree = {currentTree}
                    treeTypes = {treeTypes} 
                    branch2 = {entriesTree[branch2.entries[branch2.children[key]][id]]}
                    entriesTree = {entriesTree} 
                    entryId = {branch2.entries[branch2.children[key]][id]}
                  />
                )}
              </main>
            )}
            </div>
          }
          </div>
    
      </main>;
    }
}

window.app.components.EditorEntry = EditorEntry;

                        // <textarea key={id} onScroll = {this.handleLayout.bind(this)} 
                        //       placeholder={entriesTree[entriesTree[branch2.entries[key]].entries[id]].value} 
                        //       className="fnt-md clr-blackl3 dsp-block fix-bb w-100 bg-none brd-0 outline-none  brd-b-greyd3 pdg-tb-5px brd-dash"/>