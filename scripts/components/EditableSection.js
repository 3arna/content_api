class EditableSection extends React.Component{
  
  constructor(){
    super();
    this.handleFocus = this.handleFocus.bind(this);
    this.store = window.app.store;
    this.mainActions = window.app.mainActions;
    this.state = {
      size: 0
    };
  }

  get sectionClassNames(){
    const { branch } = this.props;
    return classNames({
      'fnt-sm list-none brd-0 pdg-0 mrg-tb-1px': true,
      'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none': true,
      'bg-greenl1 clr-black': branch.type === 'app',
      'bg-black clr-black': branch.type === 'section',
      'bg-none': branch.type === 'field',
    });
  }
  
  get iconClassNames(){
    const { branch } = this.props;
    return classNames({
      'fa fa-fw': true,
      'fa-angle-down': branch.selected,
      'fa-angle-right': !branch.selected
    });
  }
  
  handleBlur(e, branch) {
    this.save(e, branch);
  }
  
  save(e, branch){
    const { nodesTree } = this.store.data;
    if(e.target.value !== branch.name && e.target.value.length >= 3 && e.target.value.length <= 20){
      
      var newElement = {
        _id: _.startsWith(branch._id, 'edit') ? null : branch._id,
        parent: branch.parent,
        type: nodesTree[branch._id].type,
        value: e.target.value
      };
    
      if (newElement._id === null){
        console.log('saveElement', newElement);
        this.store.saveElement(newElement, branch);
      } else {
        console.log('Update');
        this.store.changeValue(newElement);
      }
    } else {
      this.showError(e);
      
      if (!!_.startsWith(branch._id, 'edit')){
        delete nodesTree[branch.parent].children[branch.sortId];
        delete nodesTree[`edit${parent}`];
      } else {
        nodesTree[branch._id].status = 'show';
      }
      console.log('blur');
    }
  }
  
  onChange(e, branch){
    //console.log(e.keyCode);
    switch(e.keyCode){
      
      //ESC
      case 27: {
        const { nodesTree }= this.store.data;
        
        if (_.startsWith(branch._id, 'edit')){ 
          delete nodesTree[branch.parent].children[branch.sortId];
          delete nodesTree[`edit${parent}`];
        } else {
          nodesTree[branch._id].status = 'show';
        }
        this.store.reset();
      }
      break;
      // ENTER
      case 13:
        this.save(e, branch);
      break;
      case 8:
      default:
        this.setState({size: e.target.value.length});
      break;
      case 37:
      case 38:
      case 39:
      case 40:
      case 16:
        return false;
      break;
    }
  }
  
  handleFocus(e){
    e.target.select();
  }
  
  showError(e){
    {e.target && e.target.value.length < 3 && this.mainActions.showError('Minimum 3 simbols length');}
    {e.target && e.target.value.length > 20 && this.mainActions.showError('Maximum 20 simbols length');}
  }
  
  render(){
    const {nodesTree, branch, context} = this.props;
    console.log('editable ', branch, nodesTree, context);
    return(
      <li key={branch._id} branchType={branch.type} className="list-none">
      
        <button  className={this.sectionClassNames} >
          {branch.type !== 'field' && !!branch.children && !!Object.keys(branch.children).length && <i className={this.iconClassNames}/>}
          
          {!!_.startsWith(branch._id, 'edit') &&
            <input onChange={(e) => this.onChange(e, branch)} onKeyUp={(e) => this.onChange(e, branch)}
              className="brd-0 bg-white clr-black outline-none pdg-tb-3px pdg-rl-5px brd-rad-2px"
              type="text" 
              autoFocus 
              onBlur={(e) => this.handleBlur(e, branch)}
              
              size={this.state.size || `Enter ${branch.type} name`.length}
              placeholder = {`Enter ${branch.type} name`}
            />
          }
          {!_.startsWith(branch._id, 'edit') && 
            <input onChange={(e) => this.onChange(e, branch)} onKeyUp={(e) => this.onChange(e, branch)}
              className="brd-0 bg-white clr-black outline-none pdg-tb-3px pdg-rl-5px brd-rad-2px"
              type="text" 
              autoFocus 
              onBlur={(e) => this.handleBlur(e, branch)}
              
              size={this.state.size || branch.value.length}
              defaultValue={branch.value}
              onFocus={this.handleFocus}
            />
          }
        </button>
        
        {!!branch.children && !!Object.keys(branch.children).length && branch.selected && 
          <ul className="mrg-0 pdg-l-20px" branchType={branch.type}>
            {Object.keys(branch.children).map(id => {
              var childBranchId = branch.children[id];
  
              if (nodesTree[childBranchId] !== undefined && nodesTree[childBranchId].status !== 'edit'){
                return !!nodesTree[childBranchId] &&
                  <app.components.Section 
                    key={ childBranchId } 
                    nodesTree={ nodesTree } 
                    branch={ nodesTree[childBranchId] } 
                    context ={(e) => context(e)}/>;
              } else {
                return !!nodesTree[childBranchId] &&
                  <app.components.EditableSection 
                    key={ childBranchId } 
                    nodesTree={ nodesTree } 
                    branch={ nodesTree[childBranchId] } 
                    context ={(e) => context(e)}
                  />;
              }
            })}
          </ul>}
      </li>
    ); 
  }
}

window.app.components.EditableSection = EditableSection;