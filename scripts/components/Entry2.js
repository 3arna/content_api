var selectedElementId = null;
var _selectedId = null;

class Entry extends React.Component{
  
  constructor(){
    super();
    
    this.mainStore = window.app.mainStore;
    this.entriesStore = window.app.entriesStore;
    this.entryActions = window.app.entryActions;
    
    this.state={
      selected: false,
      status: 'edit'
    };
    
    this.handleClick = this.handleClick.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
  }
  
  componentWillMount(){
    const { branch } = this.props;
    
    this.state.selected = branch.type === 'entry' ? false : branch.selected;
    this.state.status = branch.status;
  }
  
  get iconClassNames(){
    const { entriesTree, branch, entryId } = this.props;
    return classNames({
      'fa fa-fw clr-inherit': true,
      'fa-angle-down': entriesTree[branch._id]._selected[`${entryId === undefined ? null : entryId}${branch._id}`],
      'fa-angle-right': !entriesTree[branch._id]._selected[`${entryId === undefined ? null : entryId}${branch._id}`]
    });
  }
  
  _getEntryClassNames(isSelected, type){
    return classNames({
      'list-none txt-left': true,
      'fnt-sm list-none brd-0': true,
      'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none': true,
      'clr-white brd-black bg-none clr-black': type === 'app',
      'bg-black clr-white': type === 'section',
      'bg-none': type === 'field',
      'brd-rad-2px mrg-tb-2px bg-none': ['field', 'section', 'app', 'entity'].indexOf(type) !== -1 || isSelected,
      'hover-bg-greenl3 hover-clr-white': ['section', 'app', 'entity'].indexOf(type) !== -1 && !isSelected,
      'bg-greenl1 clr-white': ['field', 'section', 'app'].indexOf(type) !== -1 && isSelected,
      'bg-none': type === 'entry',
      'bg-none bg-white clr-blackl3 mrg-2px boxshadow brd-grey': type === 'entity' && !isSelected,
      'bg-none clr-greenl2 mrg-2px boxshadow-b-5': type === 'entity' && isSelected,
    });
  }
  
  _getWrapperClassNames(isSelected){
    return classNames({
      'brd-r-green': isSelected,
      'cursor-pointer': !isSelected,
    });
  }

  // Toggle
  handleClick(e) {
    
    
    const { entriesTree, entryId, branch } = this.props;
    //console.log('toggle', branch._id, `${entryId === undefined ? null : entryId}${branch._id}`);
  //  this.entryActions.selectElement(entryId, branch);
  ///  this.setState({ selected: !this.state.selected });
 
    entriesTree[branch._id]._selected[`${entryId === undefined 
      ? null : entryId}${branch._id}`] = !entriesTree[branch._id]._selected[`${entryId === undefined ? null 
      : entryId}${branch._id}`];
    //console.log(`${entryId}${branch._id}`);
    this.entriesStore.trigger(this.entriesStore.data);
  }
  
  /*handleSelect(e, branch) {
    //console.log('paaa');
  
    const { entryId } = this.props;
    //console.log('entryId', entryId);
    this.entryActions.setEntryId(entryId);
    

    if (branch.type !== 'field' ){
      selectedElementId = branch._id;
      window.location.hash = `/${branch.type}/${branch._id}`;
    }
  }*/
  
  handleSelect(branch, currentApp, currentTree) {
    const { entryId } = this.props;
    
    // if (_selectedId !== `${entryId === undefined ? null : entryId}${branch._id}`){
    //   _selectedId = `${entryId === undefined ? null : entryId}${branch._id}`;
    //   this.forceUpdate();
    // }
    
    
    if (_.includes(['entry', 'section'], branch.type)){
      
      selectedElementId = branch._id;
      
      if (branch._id === this.mainStore.data.selectedElementId && this.mainStore.data.selectedEntryId !== entryId){
        this.mainStore.data.selectedEntryId = entryId;
        this.mainStore.data.selectedElementId = branch._id;
        this.mainStore.trigger(this.mainStore.data);
      } else {
        this.mainStore.data.selectedEntryId = entryId;
        this.mainStore.data.selectedElementId = branch._id;
        window.location.hash = `app/${currentApp}/${currentTree}/${branch._id}`;
      }
    }
  }
  
  handleChangeStatus(e){
    //console.log('doubleClick');
    
     const { entriesTree, branch, currentSection, currentTree, currentApp, entryId } = this.props;
     //console.log(currentSection, entriesTree[branch.entry].parent);
     var _entryId = branch.entry;
     var rado = null;
     do {
       if (entriesTree[_entryId].parent === currentSection && entriesTree[_entryId].entry === entriesTree[currentSection].entry) 
       { 
         //console.log(entriesTree[_entryId].parent === currentSection, entriesTree[_entryId].entry === entriesTree[currentSection].entry);
          rado = entriesTree[_entryId].parent;
          break;
       }
       
       if (entriesTree[_entryId].entry === null) {
        rado = entriesTree[_entryId].parent;
        break;
       }
       _entryId = entriesTree[_entryId].entry;
     } while (true);
     //console.log(entriesTree[rado].value, entryId);
     entriesTree[branch._id].status = (entriesTree[branch._id].status === 'show' ? 'edit' : 'show');
     if (currentSection !== rado){
       this.mainStore.data.selectedEntryId = null;
    //   this.mainStore.data.selectedEntryId = entryId ;
       this.mainStore.data.selectedElementId = rado;
        window.location.hash = `app/${currentApp}/${currentTree}/${rado}`;
     } else {
      
       this.entriesStore.trigger(this.entriesStore.data);
     }
    //Old entriesTree[selectedElementId].status = (entriesTree[selectedElementId].status === 'show' ? 'edit' : 'show');
     
  }
  
  render(){
    const {entriesTree, branch, contextMenu, selectedElementId, entryId, currentApp, currentTree, currentSection} = this.props;
      return <li className="list-none txt-left pdg-r-5px">
        <div>
          <button >
            {branch.value === undefined ? '' : branch.value} {branch.type}
          </button>
          
          {!!Object.keys(branch.children).length && Object.keys(branch.children).map(id =>
           
            <div key={id}>
              {entriesTree[branch.children[id]].value } {entriesTree[branch.children[id]].type } 
        
            { !!branch.entries[branch.children[id]]  
              && !!Object.keys(branch.entries[branch.children[id]]).length 
              && Object.keys(branch.entries[branch.children[id]]).map(key =>
            <ul key={key}>
                  <app.components.Entry 
                    currentSection={currentSection}
                    currentApp={currentApp}
                    currentTree={currentTree}
                    key={ key } 
                    entriesTree={ entriesTree } 
                    branch={ entriesTree[branch.entries[branch.children[id]][key]] } 
                    contextMenu={ contextMenu }
                    selectedElementId={ selectedElementId }
                    entryId = {branch.entries[branch.children[id]][key]}
                  />
            </ul>
            )}
           
            </div>
          )}
        </div>  
      </li>;
  } 
}

window.app.components.Entry = Entry;