class Breadcrumbs extends React.Component{
    
    constructor(){
        super();
        this.store = window.app.store;
        this.state = {
          newValue: null,  
          value: '',
          rows: 1
        };
    }
    
    _getSectionClassNames(isSelected, type){
      return classNames({
        'list-none brd-0 brd-rad-2px bg-none pdg-tb-10px fnt-300 pdg-rl-20px': true,
        'dsp-inline valign-bot trans cursor-pointer fnt-md outline-none hover-clr-green': true,
        'clr-green': isSelected
      });
    }
  
    get iconClassNames(){
      return classNames({
        'fa fa-fw fa-angle-right clr-blackl3 valign-top pdg-t-10px mrg-t-3px dsp-inline': true,
      });
    }
    
    handleClick(e){
       const  { selectedElement, entriesTree } = this.props;
        if (selectedElement !== e._id){
             window.location.hash = `/${entriesTree[e._id].type}/${e._id}`;
        }
    }
    
    setValue(newValue){
        this.setState({newValue: newValue});
    }
    
    //jeigu simboliu skaicius mazeja, tada atimamas stulpelis.
    
    handleLayout(e){
      //console.log(e.target.scrollHeight, e.target.scrollWidth);
      // max 103 min 36
      if (e.target.scrollTop > 0){
        e.target.rows +=1;
        this.setState({rows: e.target.rows});
     //   this.handleLayout(e);
       // this.forceUpdate();
      }
      
      
    
    }
    
     handleOnChange(e){
      // const { value } = this.state;
      // if (e.target.value.length < value.length){
      //   e.target.rows -=1;
       
      // this.handleLayout(e);
         
      // }
       ////console.log('pries: ', e.target.rows , e.target.scrollTop);
       //e.target.rows -=1;
    
    // this.setState({ value: e.target.value, rows: e.target.rows });
       ////console.log('pp: ', e.target.rows, e.target.scrollTop);
    //   if (e.target.rows >= 1)
    //   this.setState({rows: e.target.rows});
     }
    
    render(){
      const {  
      selectedElementPath,
      currentSection,
      selectedElementId, 
      entriesTree, 
    } = this.props;
    //console.log( selectedElementPath);
      
      return <nav className="brd-b-grey">
          {selectedElementPath && selectedElementPath.map((branchId, index) => 
            <span key={branchId} className="dsp-inline valign-bot">
              {!!index && <i className={this.iconClassNames}/>}
              {entriesTree[branchId] &&
                <button 
                  className={this._getSectionClassNames(branchId === selectedElementId, entriesTree[branchId].type)} 
                  onClick={(e) => this.handleClick(e, e._id = branchId)}>
                    {entriesTree[branchId] && (entriesTree[branchId].value ? entriesTree[branchId].value : branchId )}
                </button>
              }
            </span>
          )}
        </nav>;
    }
}

window.app.components.Breadcrumbs = Breadcrumbs;