class EditableEntry extends React.Component{
  
  constructor(){
    super();
    this.handleClick = this.handleClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.entriesStore = window.app.entriesStore;
    this.mainActions = window.app.mainActions;
    this.store = window.app.store;
    this.entryActions = window.app.entryActions;
    this.state = {
      size: 0
    };
  }

  get entryClassNames(){
    const { branch } = this.props;
    return classNames({
      'fnt-sm list-none brd-0 pdg-0 mrg-tb-1px': true,
      'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none': true,
      'bg-greenl1 clr-black': branch.type === 'app',
      'bg-black clr-black': branch.type === 'section',
      'bg-none': branch.type === 'field',
    });
  }
  
  get iconClassNames(){
    const { branch } = this.props;
    return classNames({
      'fa fa-fw': true,
      'fa-angle-down': branch.selected,
      'fa-angle-right': !branch.selected
    });
  }
  
   handleBlur(e, branch) {
     this.save(e, branch);
  }
  
  save(e, branch){
    const { entriesTree } = this.entriesStore.data;
    const { entryId } = this.props;
    
    if(e.target.value !== branch.name && e.target.value.length >= 3 ){ //&& e.target.value.length <= 20
      var newElement = {
        _id: _.startsWith(branch._id, 'edit') ? null : branch._id,
        parent: branch.parent,
        type: entriesTree[branch._id].type === 'edit'? branch.type : entriesTree[branch._id].type,
        value: e.target.value,
        entry: !!branch.entry ? branch.entry : entryId
      };
    
      if (newElement._id === null){
        console.log('saveElement', newElement);
        this.entryActions.saveNewEntity(newElement, branch);
      } else {
        console.log('Update',  newElement);
        this.entryActions.changeValue(newElement);
      }
    } else {
      this.showError(e);
      
      if (!!_.startsWith(branch._id, 'edit')){
        delete entriesTree[branch.parent].entries[branch.sortId];
        delete entriesTree[`edit${parent}`];
      } else {
        entriesTree[branch._id].status = 'show';
      }
      console.log('blur');
    }
  }
  
  onKeyUp(e, branch){
    //console.log(e.keyCode);
    switch(e.keyCode){
      // ESC
      case 27: {
        const { entriesTree }= this.entriesStore.data;
        
        if (_.startsWith(branch._id, 'edit')){ 
          delete entriesTree[branch.parent].entries[branch.sortId];
          delete entriesTree[`edit${parent}`];
        } else {
          entriesTree[branch._id].status = 'show';
        }
        this.store.reset();
      }
      break;
      // ENTER
      case 13:
        this.save(e, branch);
      break;
      case 8:
      default:
        this.setState({size: e.target.value.length});
      break;
      case 37:
      case 38:
      case 39:
      case 40:
      case 16:
        return false;
      break;
    }
  }
  
  onChange(e, branch){
    this.setState({size: e.target.value.length});
    //this.forceUpdate();
  }
  
  
  
  
  handleClick(branchId) {
    const { entriesTree } = this.props;
    var selected = entriesTree[branchId].selected ? false : true;
    entriesTree[branchId].selected = selected;
    this.forceUpdate();
  }
  
  moveCursorToEnd(el) {
    window.setTimeout(function () {
      if (typeof el.selectionStart ==- "number") {
        el.selectionStart = el.selectionEnd = el.value.length;
      } else if (typeof el.createTextRange !== "undefined") {
        var range = el.createTextRange();
        range.collapse(false);
        range.select();
      }
    }, 1);
  }
  
  handleFocus(e){
    e.target.select();
  }
  
  showError(e){
    //console.log('Error >>', e.target.value);
   // {e.target && e.target.value.length < 3 && this.mainActions.showError('Minimum 3 simbols length');}
  //  {e.target && e.target.value.length > 20 && this.mainActions.showError('Maximum 20 simbols length');}
  }
  
  render(){
    const {entriesTree, branch, contextMenu, selectedElementId, entryId} = this.props;
    const isSelected = branch._id === selectedElementId;
    return(
      <li key={branch._id} branchType={branch.type} className="list-none">
        <div>
          <button  className={this.entryClassNames} >
            {branch.type !== 'field' && !!branch.entries && !!Object.keys(branch.entries).length && <i className={this.iconClassNames}/>}
            {false && !!_.startsWith(branch._id, 'edit') &&
              <input onChange={(e) => this.onKeyUp(e, branch)} onKeyUp={(e) => this.onKeyUp(e, branch)} 
                className="brd-0 bg-white clr-black outline-none pdg-tb-3px pdg-rl-5px brd-rad-2px"
                type="text" 
                size={this.state.size || `Enter ${branch.type} name`.length}
               //autoFocus 
                placeholder = {`Enter ${branch.type} name`}
                name="lastname" 
                onBlur={(e) => this.handleBlur(e, branch)}/>
            }
            {false && !_.startsWith(branch._id, 'edit') &&
              <input onChange={(e) => this.onKeyUp(e, branch)}  onKeyUp={(e) => this.onKeyUp(e, branch)}
             
                className="brd-0 bg-white clr-black outline-none pdg-tb-3px pdg-rl-5px brd-rad-2px"
                type="text" 
              //  autoFocus 
                 size={this.state.size || branch.value.length}
                name="lastname" 
                defaultValue={branch.value}
                onFocus={this.handleFocus}
                onBlur={(e) => this.handleBlur(e, branch)}/>
            }
          </button>
        </div>
        {!!branch.entries && (Object.keys(branch.entries).length > 0) && this.state.selected && 
          <ul key={branch.sortId}> 
            {Object.keys(branch.entries).sort().map(key =>
              (!!entriesTree[branch.entries[key]] && entriesTree[branch.entries[key]].status === 'edit' 
              ?
             
                  <app.components.EditableEntry 
                    key={ entriesTree[branch.entries[key]]._id } 
                    entriesTree={ entriesTree } 
                    branch={ entriesTree[branch.entries[key]] } 
                    contextMenu={ contextMenu }
                    selectedElementId={ selectedElementId }
                    entryId = {branch.type === 'entry' ? branch._id : entryId}
                  />
              :
                (
                (!entryId  || entryId === entriesTree[branch.entries[key]].entry || branch.type === 'entry' ) &&
                  <app.components.Entry 
                    key={ entriesTree[branch.entries[key]]._id } 
                    entriesTree={ entriesTree } 
                    branch={ entriesTree[branch.entries[key]] } 
                    contextMenu={ contextMenu }
                    selectedElementId={ selectedElementId }
                    entryId = {branch.type === 'entry' ? branch._id : entryId}
                  />
                )
              )
            )}
          </ul>
        }
      </li>
    ); 
  }
    
}


window.app.components.EditableEntry = EditableEntry;