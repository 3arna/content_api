class EditorEntry extends React.Component{
    
    constructor(){
        super();    
       
    this.entriesStore = window.app.entriesStore;
        this.entryActions = window.app.entryActions;
        this.store = window.app.store;
        this.state = {
          onlyLabel: false,
          selEntry: null,
          newValue: null,  
          value: '',
          rows: 1
        };
        this.addNewEntry = this.addNewEntry.bind(this);
        this.addNewEntity = this.addNewEntity.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
    }
    
    componentWillMount(){
      const { branch2, entriesTree} = this.props;
    
      this.state.selected = branch2.type === 'entry' ? false : entriesTree[branch2._id].selected;
      //this.state.status = branch.status;
    }
    
    get toggleIconClassNames(){
      const { entriesTree, branch2 } = this.props;
      return classNames({
        'fa fa-lg bg-white boxshadow brd-rad-50 clr-blackl3 valign-bot pos-absolute left-0 top-0': true,
        'fa-chevron-circle-down': entriesTree[branch2._id]._selected[`${branch2.entry}${branch2._id}`],
        'fa-chevron-circle-right': !entriesTree[branch2._id]._selected[`${branch2.entry}${branch2._id}`]
      });
    }
    
    _getEntryToggleIconClassNames(key){
      const { entriesTree, branch2 } = this.props;
      const selected = entriesTree[entriesTree[branch2.entries[key]]._id]._selected[`${branch2._id}${entriesTree[branch2.entries[key]]._id}`];
      return classNames({
        'fa fa-chevron-down fnt-sm clr-greyd3 dsp-block': !!selected,
        'fa fa-chevron-right fnt-sm clr-greyd3 dsp-block': !selected
      });
    }
    
      // Toggle
    handleToggle(type) {
    //  ////console.log('toggle');
    const { onlyLabel } = this.state;
    const { entryId, entriesTree, branch2, labelId } = this.props;
  ////console.log(branch2);
    if (type === 'entry'){
    entriesTree[branch2._id]._selected[`${branch2.entry === undefined 
      ? null 
      : branch2.entry}${branch2._id}`] = !entriesTree[branch2._id]._selected[`${branch2.entry === undefined 
        ? null 
        : branch2.entry}${branch2._id}`];
    }
    
    if (type !== 'entry'){
     // ////console.log('56d70e574fe47003001de03f 56cd1859c860ca0300633b47', type, branch2._id);
     // ////console.log(`${branch2._id}${type._id}`);
    
     entriesTree[type._id]._selected[`${branch2._id}${type._id}`] = !entriesTree[type._id]._selected[`${branch2._id}${type._id}`];
      // entriesTree[branch2.parent]._selected[`${entriesTree[branch2.parent].entry === undefined 
      // ? null 
      // : entriesTree[branch2.parent].entry}${entriesTree[branch2.parent]._id}`] = !entriesTree[branch2._id]._selected[`${branch2.entry === undefined 
      //   ? null 
      //   : branch2.entry}${branch2._id}`];
    }
    
      //////console.log(`${branch2.entry === undefined ? null : branch2.entry}${branch2._id}`);
      this.entriesStore.trigger(this.entriesStore.data);
      this.setState({ selected: !this.state.selected });
    }
    
    _getSectionClassNames(isSelected, type){
      return classNames({
        'list-none brd-0 brd-rad-2px bg-none pdg-tb-10px pdg-rl-20px': true,
        'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none hover-clr-green': true,
        'clr-green': isSelected
      });
    }
    
    // handleClick(e){
    //   const  { selectedElement, entriesTree } = this.props;
    //     if (selectedElement !== e._id){
    //         window.location.hash = `/${entriesTree[e._id].type}/${e._id}`;
    //     }
    // }
    
    setValue(newValue){
        this.setState({newValue: newValue});
    }
    
    //jeigu simboliu skaicius mazeja, tada atimamas stulpelis.
    
    handleLayout(e){
      //////console.log(e.target.rows, e.target.scrollHeight, e.target.scrollWidth);
      // max 103 min 36
      if (e.target.scrollTop > 0){
        e.target.rows = e.target.rows + 1;
      //  this.setState({rows: e.target.rows});
     //   this.handleLayout(e);
        
        this.forceUpdate();
        this.handleLayout(e);
      }
      
      
    
    }
    
     handleOnChange(e){
      // const { value } = this.state;
      // if (e.target.value.length < value.length){
      //   e.target.rows -=1;
       
      // this.handleLayout(e);
         
      // }
       //////console.log('pries: ', e.target.rows , e.target.scrollTop);
       //e.target.rows -=1;
    
    // this.setState({ value: e.target.value, rows: e.target.rows });
       //////console.log('pp: ', e.target.rows, e.target.scrollTop);
    //   if (e.target.rows >= 1)
    //   this.setState({rows: e.target.rows});
     }
     
    deleteElement(confirmed){
  	  const { currentTree, treeTypes } = this.props;
  	  const { selEntry } = this.state;
  	  if (confirmed) {
  	    {treeTypes[1] === currentTree && this.entryActions.deleteElement(selEntry._id, true)}
  	   // {treeTypes[0] === currentTree && this.actions.deleteElement(this.state.selectedElementId, true)}
  	   // this.store.deleteElement(this.state.selectedElementId, true);
  	  }
  	  this.setState({ confirmMenuOn: false, selEntry: null});
  	}
     
    handleConfirm(e){
	    const { branch2 } = this.props;
	    this.setState({ confirmMenuOn: true, selEntry: branch2 });
	  }
	  
	  addNewEntry(branch){
	    const { entryId  } = this.props;
	    
	    var entry = entryId;
	    {!!entry ? entry : entry = null}
      this.entryActions.saveEntry({ parent: branch._id, entry: entry});
	  }
	  
	  addNewEntity(branch){
	    const { treeTypes, entryId } = this.props;
      window.app.mainActions.addInputField(treeTypes[1], branch._id, 'entity', entryId);
	  }
    
    render(){
      
      const { selected } = this.state;
      

      const {  
      entriesTree, 
      rootEntries,
      entryId,
      branch2,
      branch,
      treeTypes,
      currentTree
    } = this.props;

    if(this.state.confirmMenuOn){
      return <app.components.ConfirmMenu confirmed={this.deleteElement.bind(this)}/>;
    }
    
     console.log(entriesTree[branch2._id]);
    
    return <main className="mrg-tb-20px pos-relative pdg-5px w-max-800px mrg-auto">
      <div className="w-100 fix-bb bg-greyl2 boxshadow">
        
        <div className="brd-t-grey">
          <i className="fa fa-times-circle fa-lg bg-white boxshadow brd-rad-50 clr-redl2 valign-bot pos-absolute right-0 top-0" 
            onClick={this.handleConfirm.bind(this)}/>
          <i className={this.toggleIconClassNames} 
            onClick={e => this.handleToggle('entry')}/>
        </div>
        
        
        
        {!!entriesTree[branch2._id]._selected[entryId] &&
          <div className="pdg-20px">
            {Object.keys(branch2.entries).map(key =>
              !!_.startsWith(key, 'field') && 
                <div key={key} className="tb w-100">
                  <label className="td brd-0 col-3 valign-top pdg-tb-10px">
                      {entriesTree[branch2.entries[key]].value}:
                  </label>
                  
                  <div className="pos-relative td w-100">
                    {Object.keys(entriesTree[branch2.entries[key]].entries).sort().map(id =>
                      !!entriesTree[entriesTree[branch2.entries[key]].entries[id]] 
                      && entryId === entriesTree[entriesTree[branch2.entries[key]].entries[id]].entry &&
                        <app.components.EditorEntityView 
                          key={id}
                          labelId = {entriesTree[branch2.entries[key]]}
                          entriesTree={entriesTree}
                          entryId={entryId}
                          data={entriesTree[entriesTree[branch2.entries[key]].entries[id]].value}
                          branch={entriesTree[entriesTree[branch2.entries[key]].entries[id]]}/>)}
                      
                    <span 
                      onClick={e => this.addNewEntity(entriesTree[branch2.entries[key]])} 
                      className="dsp-inline pdg-rl-10px pdg-tb-5px bg-greyl1 brd-grey mrg-5px clr-blackl3 boxshadow">
                        <i className="fa fa-plus clr-blackl3 dsp-block"/>
                    </span>
                  </div>
                  
                </div>
            )}
                
            { !!branch2.entries  && Object.keys(branch2.entries).length > 0 && Object.keys(branch2.entries).sort().map(key =>
             !!_.startsWith(key, 'section') && 
             <main key={key} className="pdg-t-20px">
                
                <div className="tb w-100">
                  <div className="td">
                    <i onClick={e => this.handleToggle(entriesTree[branch2.entries[key]])} className={this._getEntryToggleIconClassNames(key)}/>
                  </div>
                  <div className="td pdg-rl-5px">
                    {entriesTree[branch2.entries[key]].value}
                  </div>
                  <div className="td w-100">
                    <div className="brd-b-grey"/>
                  </div>
                  <div className="td">
                    <i className="fa fa-plus pdg-tb-10px pdg-l-10px clr-blackl3" 
                      onClick={e => this.addNewEntry(entriesTree[branch2.entries[key]])}/>
                  </div>
                </div>
                
                {false && <a className="dsp-inline" >
                  <span className="bg-blackl3 dsp-inline clr-white pdg-tb-10px pdg-rl-20px">
                   { false && <i className={this.iconClassNames} 
                    onClick = {e => this.handleToggle('entry')} /> }
                    {entriesTree[branch2.entries[key]].value}</span>
                  <i className="fa fa-plus pdg-tb-10px pdg-rl-20px" onClick={e => this.addNewEntry(entriesTree[branch2.entries[key]])}/>
                </a>}
                
                {!!entriesTree[entriesTree[branch2.entries[key]]._id]._selected[`${branch2._id}${entriesTree[branch2.entries[key]]._id}`] && Object.keys(entriesTree[branch2.entries[key]].entries).sort().map(id =>
                 !!entriesTree[entriesTree[branch2.entries[key]].entries[id]] 
                 && entriesTree[entriesTree[branch2.entries[key]].entries[id]].entry === entryId 
                 &&
               
                  <app.components.EditorEntry
                    key = {id}
                    labelis = {entriesTree[branch2.entries[key]]}
                    currentTree = {currentTree}
                    treeTypes = {treeTypes} 
                    branch2 = {entriesTree[entriesTree[branch2.entries[key]].entries[id]]}
                    entriesTree = {entriesTree} 
                    rootEntries = {rootEntries}
            
                    entryId = {entriesTree[entriesTree[branch2.entries[key]].entries[id]].type === 'entry' 
                    ? entriesTree[entriesTree[branch2.entries[key]].entries[id]]._id 
                    : entryId}
                  />
                )}
              </main>
            )}
            </div>
          }
          </div>
    
      </main>;
    }
}

window.app.components.EditorEntry = EditorEntry;

                        // <textarea key={id} onScroll = {this.handleLayout.bind(this)} 
                        //       placeholder={entriesTree[entriesTree[branch2.entries[key]].entries[id]].value} 
                        //       className="fnt-md clr-blackl3 dsp-block fix-bb w-100 bg-none brd-0 outline-none  brd-b-greyd3 pdg-tb-5px brd-dash"/>