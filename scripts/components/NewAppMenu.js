class NewAppMenu extends React.Component{
  
    constructor(){
        super();
        this.entriesStore = window.app.entriesStore;
        this.store = window.app.store;
        this.actions = window.app.actions;
        this.state = {
            appName: null,
            NewAppMenuOn: false,
            pos: { x: 0, y: 0}
        };
        this.handleClick = this.handleClick.bind(this);
    }
  
    componentWillMount(){
        const { NewAppMenuOn } = this.store.data;
        this.state.NewAppMenuOn = NewAppMenuOn;
        this.setPosition();
    }
  
    componentWillReceiveProps(){
        const { NewAppMenuOn } = this.store.data;
        this.state.NewAppMenuOn = NewAppMenuOn;
        this.setPosition();
    }
	
// 	deleteElement(e, confirm){
// 	   this.store.deleteElement(confirm);
// 	   this.setState({NewAppMenuOn: false});
// 	}
	
	setPosition(){
	    var pos = { x: 0, y: 0};
        if ((window.screen.availHeight - window.screenTop - window.innerHeight) > 0){
            pos.y = window.innerHeight / 2;
        } else {
            pos.y = (window.screen.availHeight - window.screenTop) / 2;
        }
        pos.x = window.innerWidth / 2 - 150;
        this.state.pos = pos;
	}
	
	handleClick(e, confirm){
	  const { appName } = this.state;
	  const { confirmed } = this.props;
	  confirmed(confirm, appName);
	}
	
	handleKeyUp(e){
	  console.log('key up', e);
	  console.log();
      switch(e.keyCode){
          case 13:
          {
            const { confirmed } = this.props;
    	    confirmed(confirm);
          }
          break;
          default:
            return true;
            break;
      }
    }
    
    handleChange(e){
        this.setState({ appName: e.target.value});
    }

    render(){
        
        return (
            <div className="pos-fixed top-0 left-0 w-100 h-100 bg-op-05 fadeIn z-10" > 
              <div 
                className="pos-absolute clr-white pdg-0 mrg-0 bg-none " 
                style={{top: (this.state.pos.y - 80) + 'px', left: this.state.pos.x + 'px'}}>
                  <div className="txt-center bg-greenl1 pdg-20px clr-white mrg-0">Your new App name is ?</div>
                  <div className="bg-greenl1 pdg-10px">              
                      <input 
                        onChange={this.handleChange.bind(this)} 
                        className="brd-0 w-100 fix-bb bg-white clr-black outline-none pdg-tb-5px pdg-rl-10px brd-rad-2px"
                        type="text" 
                        size="20"//{this.state.size || `Enter ${branch.type} name`.length}
                        autoFocus 
                        placeholder={`New App name`}
                        name="appName" 
                      //  onBlur={(e) => this.handleBlur(e, branch)}
                      />
                  </div>
                  <div>
                    <button className="outline-none brd-0 hover-bg-green flt-left bg-greenl1 pdg-5px clr-white fnt-md dsp-block mrg-r-1px mrg-t-1px w-150px txt-center cursor-pointer" 
                      onClick={(e) => this.handleClick(e, true)} autoFocus onKeyUp={this.handleKeyUp}>Create</button>
                    <button className="hover-bg-green flt-right bg-greenl1 pdg-5px brd-0 outline-none clr-white mrg-t-1px w-150px fnt-md txt-center cursor-pointer" 
                      onClick={(e) => this.handleClick(e, false)}>Cancel</button>
                  </div>
            </div>
          </div>
        );
    }
}

window.app.components.NewAppMenu = NewAppMenu;