class Entry extends React.Component{
  
  constructor(){
    super();
    
    this.mainStore = window.app.mainStore;
    this.entriesStore = window.app.entriesStore;
    this.entryActions = window.app.entryActions;
    
    this.state={
    };
    
    this.handleClick = this.handleClick.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
  }
  
  componentWillMount(){
  }
  
  get iconClassNames(){
    const { entriesTree, branch, entryId } = this.props;
    return classNames({
      'fa fa-fw clr-inherit': true,
      'fa-angle-down': entriesTree[branch._id]._selected[entryId],
      'fa-angle-right': !entriesTree[branch._id]._selected[entryId]
    });
  }
  
  _getEntryClassNames(isSelected, type){
    return classNames({
      'list-none txt-left': true,
      'fnt-sm list-none brd-0': true,
      'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none': true,
      'clr-white brd-black bg-none clr-black': type === 'app',
      'bg-black clr-white': type === 'section',
      'bg-none': type === 'field',
      'brd-rad-2px mrg-tb-2px bg-none': ['field', 'section', 'app', 'entity'].indexOf(type) !== -1 || isSelected,
      'hover-bg-greenl3 hover-clr-white': ['section', 'app', 'entity'].indexOf(type) !== -1 && !isSelected,
      'bg-greenl1 clr-white': ['field', 'section', 'app'].indexOf(type) !== -1 && isSelected,
      'bg-none': type === 'entry',
      'bg-none bg-white clr-blackl3 mrg-2px boxshadow brd-grey': type === 'entity' && !isSelected,
      'bg-none clr-greenl2 mrg-2px boxshadow-b-5': type === 'entity' && isSelected,
    });
  }
  
  _getWrapperClassNames(isSelected){
    return classNames({
      'brd-r-green': isSelected,
      'cursor-pointer': !isSelected,
    });
  }

  // Toggle
  handleClick(e) {
    const { entriesTree, entryId, branch } = this.props;
    
    entriesTree[branch._id]._selected[entryId] = !entriesTree[branch._id]._selected[entryId];
    console.log(entriesTree);
    this.entriesStore.trigger(this.entriesStore.data);
  }
  
  handleSelect(branch, currentApp, currentTree) {
    const { entryId } = this.props;
    
    if (_.includes(['entry', 'section'], branch.type)){
      
    //  selectedElementId = branch._id;
      
      if (branch._id === this.mainStore.data.selectedElementId && this.mainStore.data.selectedEntryId !== entryId){
        this.mainStore.data.selectedEntryId = entryId;
        this.mainStore.data.selectedElementId = branch._id;
        this.mainStore.trigger(this.mainStore.data);
      } else {
        this.mainStore.data.selectedEntryId = entryId;
        this.mainStore.data.selectedElementId = branch._id;
        window.location.hash = `app/${currentApp}/${currentTree}/${branch._id}`;
      }
    }
  }
  
  handleChangeStatus(e){
    //console.log('doubleClick');
    
     const { entriesTree, branch, currentSection, currentTree, currentApp, entryId } = this.props;
     
     {console.log(branch, currentSection)}
     //console.log(currentSection, entriesTree[branch.entry].parent);
     var _entryId = branch.entry;
     var rado = null;
     
    // do {
    //   if (entriesTree[_entryId].parent === currentSection && entriesTree[_entryId].entry === entriesTree[currentSection].entry) 
    //   { 
    //       rado = entriesTree[_entryId].parent;
    //       break;
    //   }
       
    //   if (entriesTree[_entryId].entry === null) {
    //     rado = entriesTree[_entryId].parent;
    //     break;
    //   }
    //   _entryId = entriesTree[_entryId].entry;
    // } while (true);
     
     //console.log(entriesTree[rado].value, entryId);
     branch.status = (branch.status === 'show' ? 'edit' : 'show');
     if (currentSection === rado){
    //   this.mainStore.data.selectedEntryId = null;
    // //   this.mainStore.data.selectedEntryId = entryId ;
    //   this.mainStore.data.selectedElementId = rado;
    //     window.location.hash = `app/${currentApp}/${currentTree}/${rado}`;
     } else {
      
       this.entriesStore.trigger(this.entriesStore.data);
     }
  }
  
  render(){
    const {entriesTree, branch, contextMenu, selectedEntryId,selectedElementId, entryId, currentApp, currentTree, currentSection, entries} = this.props;
    
     const isSelected = selectedEntryId === entryId && selectedElementId === branch._id;
    
      return <li className="list-none txt-left pdg-r-5px">
        <div className={this._getWrapperClassNames(isSelected)}>
        
          <button onContextMenu ={(e) => contextMenu(e, e.branch = branch, e.entryId = entryId)}  
              className={this._getEntryClassNames(isSelected, branch.type)} >
              
            { ((branch.type === 'section' && !!entries  && (Object.keys(entries).length > 0)) ||
                branch.type === 'entry'  && (Object.keys(branch.children).length > 0)
              )
              && <i className={this.iconClassNames} onClick={this.handleClick} />}
            
            <span 
              onClick={(e) => this.handleSelect(branch, currentApp, currentTree)} 
              onDoubleClick={branch.type === 'entity' && this.handleChangeStatus}>
                {branch.type === 'entry' 
                  ? <span className="fnt-sm clr-greyd2">entry</span> || <span className="dsp-inline w-200px brd-b-greenl3 valign-top mrg-t-5px pdg-t-3px "/> 
                  : branch.value
                } 
            </span>
          </button>
          {( branch.type === 'field') && 
            console.log(entries)
          }
          <ul className="pdg-l-15px">
            {branch.type === 'entry' && !!branch._selected[entryId] 
            && !!Object.keys(branch.children).length && Object.keys(branch.children).sort().map(id =>
    
              <app.components.Entry 
                currentSection={currentSection}
                currentApp={currentApp}
                currentTree={currentTree}
                key={ id } 
                entriesTree={ entriesTree } 
                branch={ entriesTree[branch.children[id]] } 
                contextMenu={ contextMenu }
                selectedElementId={ selectedElementId }
                selectedEntryId = { selectedEntryId }
                entryId = {branch._id}
                entries = {branch.entries[branch.children[id]]}
              />
            )}
            
            {((branch.type === 'section' && !!branch._selected[entryId]) || branch.type === 'field')  
              && !!entries && !!Object.keys(entries).length && Object.keys(entries).sort().map(id =>
              
              <app.components.Entry 
                currentSection={currentSection}
                currentApp={currentApp}
                currentTree={currentTree}
                key={ id } 
                entriesTree={ entriesTree } 
                branch={ entriesTree[entries[id]] } 
                contextMenu={ contextMenu }
                selectedElementId={ selectedElementId }
                selectedEntryId = { selectedEntryId }
                entryId = {entries[id]}
              />
            )}
          </ul>
        </div>  
      </li>;
  } 
}

window.app.components.Entry = Entry;