class ConfirmMenu extends React.Component{
  
    constructor(){
        super();
        this.entriesStore = window.app.entriesStore;
        this.store = window.app.store;
        this.actions = window.app.actions;
        this.state = {
            confirmMenuOn: false,
            pos: { x: 0, y: 0}
        };
        this.handleClick = this.handleClick.bind(this);
    }
  
    componentWillMount(){
        const { confirmMenuOn } = this.store.data;
        this.state.confirmMenuOn = confirmMenuOn;
        this.setPosition();
    }
  
    componentWillReceiveProps(){
        const { confirmMenuOn } = this.store.data;
        this.state.confirmMenuOn = confirmMenuOn;
        this.setPosition();
    }
	
// 	deleteElement(e, confirm){
// 	   this.store.deleteElement(confirm);
// 	   this.setState({confirmMenuOn: false});
// 	}
	
	setPosition(){
	  var pos = { x: 0, y: 0};
    if ((window.screen.availHeight - window.screenTop - window.innerHeight) > 0){
        pos.y = window.innerHeight / 2;
    } else {
        pos.y = (window.screen.availHeight - window.screenTop) / 2;
    }
    pos.x = window.innerWidth / 2 - 150;
    this.state.pos = pos;
	}
	
	handleClick(e, confirm){
	  const { confirmed } = this.props;
	  confirmed(confirm);
	}
	
	handleKeyUp(e){
	  //console.log('key up', e);
    switch(e.keyCode){
      case 13:
        const { confirmed } = this.props;
  	    confirmed(confirm);
      break;
      default:
        return true;
        break;
    }
  }

  render(){
    return (
      <div className="pos-fixed top-0 left-0 w-100 h-100 bg-op-05 fadeIn z-10"> 
        <div className="pos-absolute clr-white pdg-0 mrg-0 bg-none" 
            style={{
              top: (this.state.pos.y - 80) + 'px', 
              left: this.state.pos.x + 'px'}}>
              
                <div className="txt-center bg-greenl1 pdg-20px clr-white mrg-0">Really want to delete this element ?</div>
                <div>
                  <button autoFocus className="brd-0 hover-bg-green flt-left bg-greenl1 pdg-5px clr-white pdg-20 mrg-r-1px mrg-t-1px w-150px txt-center cursor-pointer" 
                    onClick={(e) => this.handleClick(e, true)} onKeyUp={this.handleKeyUp}>
                      Yes
                  </button>
                  <button className = "hover-bg-green flt-right bg-greenl1 pdg-5px brd-0 clr-white pdg-20 mrg-t-1px w-150px txt-center cursor-pointer" 
                    onClick={(e) => this.handleClick(e, false)}>
                      No
                  </button>
                </div>
        </div>
      </div>
    );
  }
}

window.app.components.ConfirmMenu = ConfirmMenu;