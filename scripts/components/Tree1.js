
      
class Tree extends React.Component{
  
  constructor(){
    super();
    
    this.state = {
      entryId: null,
      currentTree: null,
      scroller: null,
       branchType: null,
       active: false,
       pos : {
         x: 0,
         y: 0
       }
    };
    this.handleScroll = this.handleScroll.bind(this);
    
    
    this.mainActions = window.app.mainActions;
  }
  
  componentDidMount(){
    const { currentTree } = this.props;
   // console.log(this.props);
    this.state.currentTree = currentTree;
    this.state.scroller = ReactDOM.findDOMNode(this);
    this.forceUpdate();
    
  }

  handleContextMenu(e) {
  // console.log('contextMenu');
    this.preventDefaultContextMenu(e, true);
    
    if (!_.includes(['entry', 'entity', 'app', 'section', 'field'], e.branch.type)){
      return this.preventDefaultContextMenu(e, true);  
    }
    
    
   //console.log(e.entryId);
    this.setState({
      selectedElementId: e.branch._id,
      branchType: e.branch.type,
      active : true,
      pos: { x: e.pageX, y: (e.pageY + this.state.scroller.scrollTop)},
      entryId: (e.entryId === undefined ? null : e.entryId),
      branch: e.branch
    });
    
  }
  
  preventDefaultContextMenu(e){
    e && e.preventDefault() && e.stopPropagation();
    this.state.active && this.setState({ active : false });
  }
  
  handleScroll(e){
    this.forceUpdate();
  }
  
  _getNavItemClassNames(isActive){
    return classNames({
      'td pdg-tb-10px col-6 clr-black hover-clr-blackd3 txt-deco-none': true,
      'brd-b-green': isActive,
      'brd-b-grey cursor-pointer': !isActive 
    });
  }
  
  _generateTreeUrl(treeType){
    const {currentApp, selectedElementId} = this.props;
    return `#app/${currentApp}/${treeType}/${selectedElementId || ''}`
  }
  
  get plugUrl(){
    const {currentApp} = this.props;
    return `https://csgobets.herokuapp.com/api/json/${currentApp}`;
  }
  
  get navBackClassNames(){
    return classNames({
      'bg-blackl1 clr-white hover-bg-black fa fa-chevron-left': true,
      'hover-clr-white dsp-block pdg-15px txt-deco-none': true,
    })
  }
  
  get navPreviewClassNames(){
    return classNames({
      'bg-greenl1 clr-white hover-bg-green fa fa-plug': true,
      'hover-clr-white dsp-block pdg-15px txt-deco-none': true,
    })
  }
  
  render(){
    const {
      currentTree, 
      currentApp, 
      currentSection,
      treeTypes, 
      top, 
      nodesTree, 
      selectedElementId, 
      children, 
      errorMessage, 
      entriesTree, 
      structureTopEl,
      rootEntries
    } = this.props;
  
    return <div onScroll={this.handleScroll }
      className="over-auto pos-fixed h-100vh w-400px bg-greyl2 boxshadow brd-r-white" 
      onClick={  this.state.active  && this.preventDefaultContextMenu.bind(this)}>
      
        <nav className="tb w-100 txt-center pos-fixed top-0 w-400px z-2 bg-white">
          <a href="#dashboard" className={this.navBackClassNames}/>
          <a href={this._generateTreeUrl('structure')} className={this._getNavItemClassNames(currentTree === 'structure')}>Structure</a>
          <a href={this._generateTreeUrl('entries')} className={this._getNavItemClassNames(currentTree === 'entries')}>Entries</a>
          <a href={this.plugUrl} className={this.navPreviewClassNames} target="_blank"/>
        </nav>
      
        <ul className="list-none mrg-0 pdg-tb-60px pdg-l-20px w-100 fix-bb over-auto">
          {currentTree === treeTypes[1] && top && top.map((topId) =>  !!entriesTree[topId]  &&
            Object.keys(entriesTree[topId].entries).map(key =>
            <app.components.Entry
              currentTree={currentTree}
              currentApp={currentApp}
              currentSection={currentSection}
              key = {key}
              selectedElementId = {selectedElementId}
              entriesTree = {entriesTree}
              branch = {entriesTree[entriesTree[topId].entries[key]]}
              contextMenu = {this.handleContextMenu.bind(this)} 
              entryId = {topId}
              rootEntries = {rootEntries}
            />
          )
          )}
          {currentTree === treeTypes[0] && structureTopEl && structureTopEl.map((topId) => !!nodesTree[topId] && 
            <app.components.Section
              currentTree={currentTree}
              currentApp={currentApp}
              currentSection={currentSection}
              key={topId}
              selectedElementId={selectedElementId}
              nodesTree={nodesTree}
              branch={nodesTree[topId]}
              contextMenu={this.handleContextMenu.bind(this)} 
            />
          )}
        </ul>
        <app.components.DropDownMenu 
          currentTree = { currentTree }
          treeTypes = { treeTypes }
          active={this.state.active}
          pos={this.state.pos} 
          branchType={this.state.branchType} 
          branchId={this.state.selectedElementId}
          entryId = {this.state.entryId} 
          branch = {this.state.branch}
          
          />
        {errorMessage && 
          <div 
            onClick={false} 
            className="pos-absolute w-380px fix-bb pdg-rl-20px pdg-tb-10px bg-red z-3 right-0 mrg-auto left-0 op-08 clr-white" 
            style={{ bottom: `-${this.state.scroller.scrollTop}px`}}>
              {errorMessage}
              <span className="pos-absolute fnt-sm brd-rad-50 right-0 mrg-r-10px bg-white clr-red w-20px txt-center h-20px">x</span>
          </div>}
    </div>;
    
  }
    
}

window.app.components.Tree = Tree;