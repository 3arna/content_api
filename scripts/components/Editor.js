class Editor extends React.Component{
    
    constructor(){
        super();
        this.store = window.app.store;
        this.state = {
          newValue: null,  
          value: '',
          rows: 1
        };
        this.entryActions = window.app.entryActions;
        this.addNewEntry = this.addNewEntry.bind(this);
    }
    
    _getSectionClassNames(isSelected, type){
      return classNames({
        'list-none brd-0 brd-rad-2px bg-none pdg-tb-10px pdg-rl-20px': true,
        'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none hover-clr-green': true,
        'clr-green': isSelected
      });
    }
  
    get iconClassNames(){
      return classNames({
        'fa fa-fw fa-angle-right clr-blackl3 valign-top pdg-t-10px mrg-t-3px dsp-inline': true,
      });
    }
    
    handleClick(e){
      const  { selectedElement, entriesTree } = this.props;
      if (selectedElement !== e._id){
        window.location.hash = `/${entriesTree[e._id].type}/${e._id}`;
      }
    }
    
    setValue(newValue){
      this.setState({newValue: newValue});
    }
    
    //jeigu simboliu skaicius mazeja, tada atimamas stulpelis.
    
    handleLayout(e){
      if(e.target.scrollTop > 0){
        e.target.rows +=1;
        this.setState({rows: e.target.rows});
      }
    }
    
     /*handleOnChange(e){
      const { value } = this.state;
      
      if (e.target.value.length < value.length){
        e.target.rows -=1;
        this.handleLayout(e);
      }
      
      //console.log('pries: ', e.target.rows , e.target.scrollTop);
      e.target.rows -=1;
    
      this.setState({ value: e.target.value, rows: e.target.rows });
      //console.log('pp: ', e.target.rows, e.target.scrollTop);
      if(e.target.rows >= 1)
      this.setState({rows: e.target.rows});
     }*/
     
    addNewEntry(branch){
	    const { entryId  } = this.props;
	    
	    let entry = entryId;
	    {!!entry ? entry : entry = null}
      this.entryActions.saveEntry({ parent: branch._id, entry: entry});
	  }
    
    render(){
      
      const {  
        currentSection,
        currentTree, 
        treeTypes, 
        selectedElementId, 
        entriesTree, 
        selectedEntryId,
        rootEntries,
        selectedElements
    } = this.props;
      
      /*//console.log(!!entriesTree[selectedElementId] && entriesTree[selectedElementId].type === 'section' 
            && currentTree === treeTypes[1]
            && !!selectedElementId 
            && !!entriesTree[selectedElementId] );*/
      return <div className="fix-bb w-100 h-min-100vh bg-greyl1" style={{paddingLeft: '400px'}}>
        
        <app.components.Breadcrumbs {...this.props} />
        
        <section className="pdg-30px">
        
          {false && !!entriesTree[selectedElementId] && entriesTree[selectedElementId].type === 'section' && currentTree === treeTypes[1] && 
            <div className="tb w-100">
              <div className="td">
                <i className="fa fa-chevron-down fnt-sm clr-greyd3 dsp-block"/>
              </div>
              <div className="td pdg-rl-5px">
                {!!entriesTree[selectedElementId] && entriesTree[selectedElementId].value} 
              </div>
              <div className="td w-100">
                <div className="brd-b-grey"/>
              </div>
              <div className="td">
                <i className="fa fa-plus pdg-tb-10px pdg-l-10px clr-blackl3" 
                  onClick={e => this.addNewEntry(entriesTree[selectedElementId])}/>
              </div>
            </div>}
            
          {!!entriesTree[selectedElementId] && entriesTree[selectedElementId].type === 'section' && currentTree === treeTypes[1] && <div 
            className="pos-absolute bg-blackl3 pdg-tb-10px pdg-rl-10px clr-white right-0 top-0 fnt-sm cursor-pointer"
            onClick={e => this.addNewEntry(entriesTree[selectedElementId])}>
            <i className="fa fa-plus pdg-r-10px"/>
              add new entry 
          </div>}
        
          {!!entriesTree[selectedElementId] && entriesTree[selectedElementId].type === 'section' 
            && currentTree === treeTypes[1]
            && !!selectedElementId 
            && !!entriesTree[selectedElementId] 
            && Object.keys(entriesTree[selectedElementId].entries).map((topId) => 
              (!selectedEntryId || !!_.startsWith(topId, 'entry') 
             && selectedEntryId === entriesTree[entriesTree[selectedElementId].entries[topId]].entry
           ) 
           &&
                <app.components.EditorEntry
                  key = {topId}
                  currentTree = {currentTree}
                  treeTypes = {treeTypes} 
                  branch2 = {entriesTree[entriesTree[selectedElementId].entries[topId]]}
                  entriesTree = {entriesTree} 
                  rootEntries = {rootEntries}
                  selectedElements = {selectedElements}
                  entryId = {entriesTree[entriesTree[selectedElementId].entries[topId]].type === 'entry' 
                    ? entriesTree[entriesTree[selectedElementId].entries[topId]]._id 
                    : entriesTree[entriesTree[selectedElementId].entries[topId]]}
                />
          )}
          {!!entriesTree[selectedElementId] && entriesTree[selectedElementId].type === 'entry'
            && currentTree === treeTypes[1]
            && !!selectedElementId 
            && !!entriesTree[selectedElementId] &&
              <app.components.EditorEntry
                key = {selectedElementId}
                currentTree = {currentTree}
                treeTypes = {treeTypes} 
                branch2 = {entriesTree[selectedElementId]}
                entriesTree = {entriesTree} 
                rootEntries = {rootEntries}
                selectedElements = {selectedElements}
                entryId = {selectedElementId}
              />
          }
        </section>
      </div>;
    }
}

window.app.components.Editor = Editor;