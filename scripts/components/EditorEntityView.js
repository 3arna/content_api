var selectedElementId = null;

class EditorEntityView extends React.Component{
  
    constructor(){
        super();
        this.entryActions = window.app.entryActions;
        this.state=  {
          status: false,
          rows: 1, 
          value: '',
          confirmMenuOn: false,
          _textarea: null
        };
        this.handleFocus = this.handleFocus.bind(this);
      this.entriesStore = window.app.entriesStore;
      this.mainActions = window.app.mainActions;
      this.entryActions = window.app.entryActions;
      this.store = window.app.store;
      this.handleChangeStatus = this.handleChangeStatus.bind(this);
    }
  
    componentWillMount(){
      const { data } = this.props;
      this.state.value = data;
    }
  
    componentWillReceiveProps(){
     //console.log(this.state._textarea, this.state.rows);
     
    // this.state.rows = this.state._textarea.scrollHeight/18;
    }
    
    componentDidUpdate(){
      
      // if (this._textarea && this._textarea.height){
      // console.log(this._textarea.scrollHeight);
      // this.state.rows = this._textarea.scrollHeight/18;
      // }
     // this.setState({rows:   this._textarea.scrollHeight/18});
    }
    
    componentDidMount(){
      this.state._textarea = this._textarea;
          var dataField = ReactDOM.findDOMNode(this._textarea);
         
    //   console.log(dataField.rows, dataField.scrollHeight, dataField.scrollWidth);
    //   // max 103 min 36

       //this.setState({rows:   this._textarea.scrollHeight/18});
    }
    
  handleBlur(e, branch) {
     this.save(e, branch);
  }
  
  save(e, branch){
    
    const { entriesTree } = this.entriesStore.data;
    const { entryId } = this.props;
    
    if(e.target.value !== branch.name && e.target.value.length >= 3 ){ //&& e.target.value.length <= 20
      var newElement = {
        _id: _.startsWith(branch._id, 'edit') ? null : branch._id,
        parent: branch.parent,
        type: entriesTree[branch._id].type === 'edit'? branch.type : entriesTree[branch._id].type,
        value: e.target.value,
        entry: !!branch.entry ? branch.entry : entryId
      };
    
      if (newElement._id === null){
        //console.log('saveElement', newElement);
        this.entryActions.saveNewEntity(newElement, branch);
      } else {
        //console.log('Update',  newElement);
        this.entryActions.changeValue(newElement);
      }
    } else {
      this.showError(e);
      
      if (!!_.startsWith(branch._id, 'edit')){
        delete entriesTree[branch.parent].entries[branch.sortId];
        delete entriesTree[`edit${parent}`];
      } else {
        entriesTree[branch._id].status = 'show';
      }
      //console.log('blur');
    }
    this.setState({rows : e.target.scrollHeight/18});
  }
  
  showError(e){
    //console.log('Error >>', e.target.value);
   // {e.target && e.target.value.length < 3 && this.mainActions.showError('Minimum 3 simbols length');}
  //  {e.target && e.target.value.length > 20 && this.mainActions.showError('Maximum 20 simbols length');}
  }
  
  onKeyUp(e, branch){
  //  const _scaleHeight = (e, onChange) => {
      e.target.style.height = 'auto';
      e.target.style.height = (e.target.scrollHeight+5) + 'px';
      //!!onChange && onChange(e);
      //  };
    //console.log(e.keyCode);
    switch(e.keyCode){
      // ESC
      case 27: {
        const { entriesTree }= this.entriesStore.data;
        
        if (_.startsWith(branch._id, 'edit')){ 
          delete entriesTree[branch.parent].entries[branch.sortId];
          delete entriesTree[`edit${parent}`];
        } else {
          entriesTree[branch._id].status = 'show';
        }
        this.store.reset();
      }
      break;
      // ENTER
      //TAB ???
      case 9:
        this.save(e, branch);
      break;
      case 8:
      default:
        this.setState({size: e.target.value.length});
      break;
      case 37:
      case 38:
      case 39:
      case 40:
      case 16:
        return false;
      break;
    }
    
      this.setState({ value: e.target.value});
  }
  
  handleChangeStatus(e){
    const { entriesTree, branch } = this.props;
    entriesTree[branch._id].status = (entriesTree[branch._id].status === 'show' ? 'edit' : 'show');
    this.entriesStore.trigger(this.entriesStore.data);
   //this.setState({ status: entriesTree[branch._id].status});
    //this.forceUpdate();
  }
  
  handleFocus(e){
    //console.log('foxus');
    e.target.select();
  }
  
  handleConfirm(e){
	  const { branch } = this.props;
	  this.setState({ confirmMenuOn: true});
	}
	
	deleteElement(confirmed){
	  const {  branch } = this.props;
	  if (confirmed) {
	     this.entryActions.deleteElement(branch._id, true);
	   // {treeTypes[0] === currentTree && this.actions.deleteElement(this.state.selectedElementId, true)}
	   // this.store.deleteElement(this.state.selectedElementId, true);
	  }
	  this.setState({ confirmMenuOn: false });
	}
  
  render(){
    

    const { rows, value, confirmMenuOn} = this.state;
    const { data, branch } = this.props;
    
    if(confirmMenuOn){
      return <app.components.ConfirmMenu confirmed={this.deleteElement.bind(this)}/>;
    }
    
    {false && <i className="td fa fa-times pdg-0px pull-right" onClick = {this.handleConfirm.bind(this)} />}
    {false && <i className="td fa fa-edit pdg-0px pull-right" onClick={branch.type === 'entity' && this.handleChangeStatus}/>}
    
    {false && <textarea ref={(c) => this._textarea = c} 
      onDoubleClick={branch.type === 'entity' && this.handleChangeStatus}
      value={data}
      readOnly
      rows={rows}
      className="fnt-md clr-blackl3 dsp-block fix-bb w-100 bg-none brd-0 outline-none z-3 brd-b-greyd3 pdg-tb-5px brd-dash h-auto"/>}
    
    // Fiel to show
    if (branch.status === 'show')
      return <span className="dsp-inline pdg-rl-10px bg-greyl1 brd-grey mrg-5px clr-blackl3 boxshadow pdg-tb-5px">{data}</span>
    // New value field
    if (branch.status === 'edit' && branch.value.length === 0) {
      return(
        <textarea  //onChange = {_scaleHeight} 
          onChange={(e) => this.onKeyUp(e, branch)} 
          onKeyUp={(e) => this.onKeyUp(e, branch)} 
          onBlur={(e) => this.handleBlur(e, branch)}
          autoFocus
          placeholder = {`Enter ${branch.type} name`}
          rows = {rows}
          className="fnt-md clr-blackl3 dsp-block fix-bb w-100 bg-none brd-0 outline-none z-3 brd-b-greyd3 pdg-tb-5px brd-dash"/>
      );
    }
    // Editable value field
    
    if (branch.status === 'edit' && branch.value.length > 0) {
      return(
         
        branch.status === 'edit' && branch.value.length > 0 &&
        
        <textarea  //onChange = {_scaleHeight} 
          
          onChange={(e) => this.onKeyUp(e, branch)} 
          onKeyUp={(e) => this.onKeyUp(e, branch)} 
          onBlur={(e) => this.handleBlur(e, branch)}
          autoFocus
          value = {value}
          onFocus={e => this.handleFocus(e)}
          rows = {rows}
          className="fnt-md clr-blackl3 dsp-block fix-bb w-100 bg-none brd-0 outline-none z-3 brd-b-greyd3 pdg-tb-5px brd-dash"/>
      
      );
    }
  }
}

window.app.components.EditorEntityView = EditorEntityView;