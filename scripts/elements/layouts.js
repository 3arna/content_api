const centered = ({children, className, errorMessage}) =>
  <div className={`w-100 h-min-100vh bg-black clr-white fnt-main tb ${className}`}>
    <div className="td">{children}</div>
    {errorMessage && 
      <div className="pdg-tb-10px pdg-rl-20px bg-redl2 clr-white pos-fixed top-0 left-0 right-0 mrg-auto w-max-300px">{errorMessage}</div>}
  </div>

window.app.elements.layouts = { centered };