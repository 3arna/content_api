const _logoutButtonClassNames = classNames({
  'fa fa-power-off fa-lg pos-fixed top-0 right-0 cursor-pointer': true,
  'brd-l-blackl3 brd-b-blackl3 bg-black pdg-20px clr-white hover-bg-redl1': true,
})

const logout = ({onClick}) => 
  <i onClick={onClick} className={_logoutButtonClassNames}/>


window.app.elements.buttons = { logout }