var request = window.superagent;

window.app.entryActions = Reflux.createActions([
  'changeStatus', 
  'reset', 
  'saveNewEntity', 
  'saveEntry', 
  'changeValue', 
  'deleteElement', 
  'setEntryId', 
  'loadData', 
  'selectElement'
]);

window.app.entriesStore = Reflux.createStore({
  listenables: [window.app.entryActions],
  
  mainActions: window.app.mainActions,
  
  data: {
    sections: [],
    entriesTree: {},
    top: null,
    topEntries: {},
    doomElements: [],
    entryId: null, 
    rootEntries: {},
    selectedElements: {}
  },
  
  store: window.app.store,
  
    // // API HELPERS
  
  getApiUrl: (url) => {
    return 'https://csgobets.herokuapp.com/api/content/' + url;
  },
  
  onSelectElement(entryId, branch){
    const { entriesTree } = this.data;
    entriesTree[branch._id].selected != entriesTree[branch._id].selected;
    //branch.selectedElements[`{entryId] = true;
    // const { selectedElements } = this.data;
    // if (!selectedElements[`${entryId}${branch._id}`]) { 
    //   selectedElements[`${entryId}${branch._id}`] = true; }
    // else  { 
    //   delete selectedElements[`${entryId}${branch._id}`]; }
    //   //console.log(selectedElements);
      this.trigger(this.data);
  },
  
  onChangeStatus(e, branch){
    if (branch.type === 'entity'){
      const { entriesTree } = this.data;
      entriesTree[branch._id].status = (entriesTree[branch._id].status === 'show'? 'edit' : 'show');
      this.trigger(entriesTree);
    }
  },
  
  // getPathTo: function(branch){
   
  //   var path = [];
  //   var currentNode = branch;
  
  //   if (this.data.entriesTree[currentNode]){
  //     while (this.data.entriesTree[currentNode].parent !== 1) {
  //       path.unshift(currentNode);
  //       currentNode = this.data.entriesTree[currentNode].parent;
  //     } 
  //     path.unshift(currentNode);
  //   }
  //   this.data.selectedElementPath = path;
  // },

  init: function() {
    //console.log('ENTRIES STORE MOUNTING');
//     request.get(this.getApiUrl(''))
//       .set("Authorization", "56c1002404c4da230899971e")
// .set("From", "56b2bde3e4b0102fef2432b6")
//       .end((err, apiRes) => {
//         // THE SAME: NEED TO CHECK !!!!
//         if(err || !apiRes.body.ok){
//           this.mainActions.showError(apiRes.body.msg || err);
//         //OLD VERSION return this.setData({errorMessage: apiRes.body.msg || err});
//         }
//         this.data.sections = apiRes.body.data;
//         this.setEntryTemplates();
//         this.mainActions.loadedStore('entriesStore', this);

//         //console.log('ENTRIES STORE FIRST TIME LOADED AND MADE GOTO');
//       // this.data.entriesStore.setData();
//         //window.app.store.goTo(window.location.hash.replace(/^#\/?|\/$/g, '').split('/'));
//       //  this.trigger(this.data);
//       });
  },
  
  // onLoadData: function(){
  //   const reqOpts = {
  //     method:  'get', 
  //     table: 'content',
  //     url:  ''
  //   };
    
  //   // tai reikia atlikti i mainStore, using 
  //   this.mainActions.callAPI(reqOpts, (result) => {
  //       //console.log(result);
  //       if (result.ok){
  //         //console.log('I just got store data !!! ', result);
  //         this.data.sections = result.data;
  //         this.setEntryTemplates();
  //         this.mainActions.loadedStore('entries', this);
  //         //console.log('ENTRIES STORE FIRST TIME LOADED AND MADE GOTO');
  //         // this.trigger(this.data);
  //     }
  //   });
  // },
  
  onLoadData: function(mainTop, maintree, createNode){
    //console.log('STORE DATA MODIFIED', {data: this.data});
    let { top, sections, entriesTree, topEntries, rootEntries } = this.data;
  
    var tree = maintree;
    sections = tree;
    entriesTree = tree;
    top = [];

    // console.log(mainTop, maintree);
  
    // Gets main entries
    if (tree ) {
      Object.keys(tree).map((topId) => {
       // console.log(tree[topId]);
        if (tree[topId] && tree[topId].entry === null  && tree[topId].type === 'entry') {
          top.push(topId);
          console.log(entriesTree[entriesTree[topId].parent].sortId, entriesTree[topId].parent);
          //entriesTree[topId].entries[entriesTree[entriesTree[topId].parent].sortId] = entriesTree[topId].parent;
          //entriesTree[entriesTree[topId].parent].entry = topId;
          //entriesTree[entriesTree[topId].parent].entries = entriesTree[entriesTree[topId].parent].children;
          rootEntries[topId] = tree[topId];
        
        }
      });
    } 
    
    //Sukuria visus entries/entities
      Object.keys(sections).map(key => {
        
        var section = sections[key];
      if (section.entry !== null && (section.type === 'entry' || section.type === 'entity')){
        rootEntries[section._id] = section;
            
        //console.log(section.value + ' ' + section._id);
        if (section.entry === null ) {
            topEntries[section._id] = section._id;
            entriesTree[section.parent].entries[entriesTree[section._id].sortId] = section._id;

        }
        //                                    _id,      parentID,         value, children,      type,       entryId
        entriesTree[section._id] = window.app.mainStore.createNode(section._id, section.parent, section.value, null, section.type, section.entry);
        entriesTree[section._id].entries = entriesTree[section.parent].children;
      }
    });
    
    Object.keys(entriesTree).map(entryId => {
      var item = entriesTree[entryId];
        if (item.entry !== null)
        {
          if (rootEntries[item.entry] !== undefined ) {
            rootEntries[item.entry].entries[rootEntries[item._id].sortId] = item._id;
          } else {
            //console.log('Rubish: ', item);
          }
          ////console.log(rootEntries, item._id, item.entry, rootEntries[item.entry]);
          entriesTree[item.parent].entries[entriesTree[item._id].sortId] = item._id;
        } else {
                  //           entriesTree[item._id].entries = entriesTree[item.parent].children;
                  // //console.log(entriesTree[item.parent].children, item._id);
        }
    });
    //-------------------------------
    
    this.data.entriesTree = entriesTree;
 
    this.data.top = top;
    
    //console.log('ENTRIES STORE DATA MODIFIED', {data: this.data});
   
    this.trigger(this.data);
    this.mainActions.loadedStore('entries', this, entriesTree);
    //console.log('rootEntries', this.data.rootEntries);
    //console.log('ENTRIES STORE FIRST TIME LOADED AND MADE GOTO');
  },
  
  setEntryTemplates: function() {
   
    var tree = {};
    var top = [];
    const { rootEntries, sections } = this.data;
    
    sections.map(section => {
      if (section.parent === undefined){
        
        //select and store all type "app" ids in to top array
        section.type === 'app' && top.push(section._id);
        tree[section._id] = window.app.mainStore(section._id, 1, section.value, null, section.type);
      } else {
            var entry = window.app.mainStore(section._id, section.parent, section.value, null, section.type, section.entry);
            if ((section.type === 'entry' || section.type === 'entity')) {
              rootEntries[section._id] = entry;
            } 
            tree[section._id] = entry;
            //tree[section._id] = window.app.mainStore(section._id, section.parent, section.value, null, section.type, section.entry);
      }
    });
            //console.log(tree);
    sections.map(section => {
      if (section.parent !== undefined){
        if (section.entry === undefined ){
          if (section.type !== 'entry' && section.type !== 'entity'){
            tree[section.parent].children[tree[section._id].sortId] = section._id;
          }
        }
      }
    });
    //console.log(tree);
    // Add Edit field
    ////console.log('patikrinimas: ', tree, tree['edit']);
    tree['1'] = window.app.mainStore('1', '1', 'Element name ..', null, 'none');
  
    tree['1'].status = 'edit';
    //tree['565a3a62e4b030fba33dc046'].children.push('edit');
    
    this.data.entriesTree = tree;
 
    this.data.top = top;
    
    //console.log('STORE DATA MODIFIED', {data: this.data});
 
    this.trigger(this.data);
    this.setData();
  },
  
  
  onSetEntryId(id){
    this.data.entryId = id;
  },
  
  onReset: function(){
    this.trigger(this.data);
  },

  setData: function() {
    
    const { top, sections, entriesTree, topEntries } = this.data;
    
    var tree = {};
    var mainSections = [];
    // Gets main sections 
    
    console.log(top);
    
    if (top ) {
      top.map((topId) => {
        // ////console.log(topId);
      // tree[topId] = entriesTree[topId];
         entriesTree[topId].entries = entriesTree[topId].children;
      });
    } 
    
    //console.log(entriesTree);

    
    //Sukuria visus entries/entities
      sections.map(section => {
      if (section.type === 'entry' || section.type === 'entity'){
        
         // ----- Siuksliu termintorius (trina visus entrius/enticius), naudoti tik extra atvejais ---- //
              // //console.log('<', section.value +' ' + section._id);
              // const reqOpts = {
              //   method: 'delete', 
              //   url: `${section.type}/${section._id}`
              // };
              // this.apiCallHandler(reqOpts, (data) => {
              //   //console.log('ex', data);
              // });  
         //--------------------------                     ------------------------------//
        
        //console.log(section.value + ' ' + section._id);
        if (section.entry === null ) {
            topEntries[section._id] = section._id;
            entriesTree[section.parent].entries[entriesTree[section._id].sortId] = section._id;

        }
        //                                    _id,      parentID,         value, children,      type,       entryId
        entriesTree[section._id] = window.app.mainStore(section._id, section.parent, section.value, null, section.type, section.entry);
        entriesTree[section._id].entries = entriesTree[section.parent].children;
      }
    });
    
    Object.keys(entriesTree).map(entryId => {
      var item = entriesTree[entryId];
        if (item.entry !== null)
        {
          entriesTree[item.parent].entries[entriesTree[item._id].sortId] = item._id;
        } else {
                  //           entriesTree[item._id].entries = entriesTree[item.parent].children;
                  // //console.log(entriesTree[item.parent].children, item._id);
        }
    });
    //console.log(entriesTree);
    //--------------------------------
    
    // Add Edit field
    ////console.log('patikrinimas: ', tree, tree['edit']);
    entriesTree['1'] = window.app.mainStore('1', '1', 'Element name ..', null, 'none');
  
    entriesTree['1'].status = 'edit';
    //tree['565a3a62e4b030fba33dc046'].children.push('edit');
    
    this.data.entriesTree = entriesTree;
 
    this.data.top = top;
    
    //console.log('ENTRIES STORE DATA MODIFIED', {data: this.data});
    
    this.trigger(this.data);
  },
  
  onSaveEntry: function(element){
    
    const reqOpts = {
      method:  'post', 
      table: 'content',
      url:  'entry', 
      body: {
        parent: element.parent,
        entry: element.entry
      }
    };
    
    this.mainActions.callAPI(reqOpts, 
      (result) => {
        var newData = result.data;
        const { entriesTree, rootEntries } = this.data;
        var newElement =  window.app.mainStore.createNode(newData._id, newData.parent, newData.value, null, newData.type, newData.entry);
        entriesTree[newData._id] = newElement;
        
        rootEntries[newElement._id] = window.app.mainStore.createNode(newData._id, newData.parent, newData.value, null, newData.type, newData.entry);
        if (newElement.entry !== null && rootEntries[newElement.entry].entry !== null) rootEntries[newElement.entry].entries[rootEntries[newElement._id].sortId] = newElement._id; 
        
        entriesTree[newElement.parent].entries[newElement.sortId] = newData._id;
        entriesTree[newElement._id].entries = entriesTree[newElement.parent].children;
        this.trigger(this.data);
      });
     
  },
  
  onSaveNewEntity: function(element, branch){
    const { entriesTree, rootEntries } = this.data;

    entriesTree[branch._id].status = 'show';
    entriesTree[branch._id].value = element.value;
    this.trigger(this.data);
    
    const reqOpts = {
      method:  'post', 
      table: 'content',
      url:  'entity',
      body: {
        value:  element.value,
        type:  element.type,
        parent:  element.parent,
        entry: element.entry
      }
    };
    
    this.mainActions.callAPI(reqOpts,  
      (result) => {
        var newData = result.data;
        //console.log(newData);
        var newElement = entriesTree[newData._id] = window.app.mainStore.createNode(newData._id, newData.parent, newData.value, null, newData.type, newData.entry);
        entriesTree[newElement.parent].entries[newElement.sortId] = newElement._id;
       // entriesTree[newElement.entry].realEntries[newElement.sortId] = newElement._id;
        
        //console.log('bug', rootEntries, newElement);
        rootEntries[newElement._id] = entriesTree[newData._id] = window.app.mainStore.createNode(newData._id, newData.parent, newData.value, null, newData.type, newData.entry);
        rootEntries[newElement.entry].entries[newElement.sortId] = newElement._id;

        delete entriesTree[branch.parent].entries[branch.sortId];
        delete entriesTree[branch._id];
        this.trigger(this.data);
      });
      
  },
  
  onChangeValue: function(element) {
      
    const { entriesTree } = this.data;
    var oldValue = entriesTree[element._id].value;
    entriesTree[element._id].value = element.value;
    entriesTree[element._id].status = 'show';
    this.trigger(this.data);
    
    const reqOpts = {
      method:  'post', 
      table: 'content',
      url: element.type,
      body: {
        _id: element._id,
        parent: element.parent,
        value: element.value,
        entry: element.entry
      }
    };
    
    this.mainActions.callAPI(reqOpts,  
      (result) => {
        if (!result.ok){
          entriesTree[element._id].value = oldValue;
          this.trigger(this.data);
        }  
      });
  },
  
  apiCallHandler: function(opts, next){
    request(opts.method || 'get', this.getApiUrl(opts.url))
      .send(opts.body)
      .end((err, apiRes) => {
        
        console.warn(`API CALLED to "/${opts.url}"`, {body: apiRes.body});
       
        if (!apiRes.body.ok || err){
          this.mainActions.showError(apiRes.body.msg || err);
          this.init();
        } else next(apiRes.body.data);
      });
  },
  
  onDeleteElement : function(id, update) {
    
    let { doomElements, entriesTree, rootEntries } = this.data;
    var newDoomElements = [];
    
    this.getAllElements(newDoomElements, id, id);
    newDoomElements.unshift(id);
    
    delete entriesTree[entriesTree[id].parent].entries[entriesTree[id].sortId];
    if (rootEntries[id].entry !== null)
      delete rootEntries[rootEntries[id].entry].entries[rootEntries[id].sortId];
    this.trigger(this.data);
    
    
    //console.log('id', newDoomElements.map((id) => entriesTree[id]));
    
    this.data.doomElements = _.concat(newDoomElements, doomElements);    
    // newDoomElements.map((doomId) => this.removeFromOldParent(entriesTree[doomId]));
    // this.trigger(this.data);
    //console.log('trinami objektai: ', this.data.doomElements);
     //this.removeFromOldParent();
    this.exterminate();
  },
  
  exterminate: function(){
    const { doomElements, entriesTree, rootEntries } = this.data;
    //console.log('likes masyvas')
    if(doomElements.length){
    var ex = doomElements.pop();
    
    const reqOpts = {
      method: 'delete', 
      table: 'content',
      url: `${entriesTree[ex].type}/${ex}`
    };
    
    this.mainActions.callAPI(reqOpts,  
      
        (result) => {
        delete entriesTree[ex];
        delete rootEntries[ex];
        //console.log('ex', result.data);
        this.exterminate();
        
      });  
    } else{
      this.mainActions.deselectElement();
    }
  },
  
  // Gauna visus elementus ir kartu pasalina is NodesTree
  getAllElements: function(doomElements, id, mainEntry){
    
    const { entriesTree, rootEntries } = this.data;
    
    if (rootEntries[id] && Object.keys(rootEntries[id].entries).length > 0){
      Object.keys(rootEntries[id].entries).map((key) => {
        doomElements.push(rootEntries[id].entries[key]);
        delete entriesTree[entriesTree[id].parent].entries[entriesTree[id].sortId];
        if (rootEntries[id].entry !== null)
          delete rootEntries[rootEntries[id].entry].entries[rootEntries[id].sortId];
        this.getAllElements(doomElements, rootEntries[id].entries[key], mainEntry);
      });
    }
  },
  
  removeFromOldParent: function(child){
    var tree = this.data.entriesTree;
    
    if (child.type === 'entity' || child.type === 'entry') {
      //console.log(`just deleted: ${child.value}`);
      delete tree[child.parent].entries[child.sortId];
    }
   // //console.log('parent', tree[child.parent].entries);
  },

  
  
  
});