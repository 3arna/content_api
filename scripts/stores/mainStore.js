var request = window.superagent;

window.app.mainActions = Reflux.createActions([ 
  'register',
  'login', 
  'logout',
  'switchToUser', 
  'loadUserApps', 
  'showError', 
  'loadedStore', 
  'changeTreeTo', 
  'goTo',
  'redirectTo',
  'callAPI',
  'addInputField',
  'deselectElement'
]);

window.app.mainStore = Reflux.createStore({
  listenables: [window.app.mainActions],
  
  entriesStore: null,
  
  data: {
    storesList: {
      'structure': {
        loaded: false, store: null
      },
      'entries': {
        loaded: false, store: null
      }
    },
    userApps: [],
    
    user: { 
      authTokken: !!reactCookie.load('userTokken') && reactCookie.load('userTokken'), 
      currentApp:  null
    },
            
    treeTypes : [
      'structure',
      'entries'
    ],
    
    selectedElementId: null,
    selectedElementPath: [],
    loaded: false,
    errorMessage: null,
    isEntryTree: null, 
    currentTree: null,
    currentApp: null,
    currentSection: null,
    selectedEntryId: null
  },

  init: function() {
    this.data.currentTree = this.data.treeTypes[0];
   // this.entriesStore = window.app.entriesStore;
   // if (this.data.loaded){
  
   // }
  },
  
  getApiUrl: (table, url) => {
    return `https://csgobets.herokuapp.com/api/${table}/${url}`;
  },
  
  onRegister(userName, password){
    const reqOpts = {
      method:  'post', 
      table: 'user',
      url:  '', 
      body: {
        username: userName,
        password: password
      }
    };
    
    this.onCallAPI(reqOpts, 
      (body) => {
        if (body.ok){
          reactCookie.save('userTokken', body.data.token, { path: '/' }); 
          this.data.user = { authTokken: body.data.token, currentApp: null };
          // change address ...
          window.location.hash = `dashboard`;
          this.trigger(this.data);
        } 
      });
  },
  
  onLogin(userName, password){
    const reqOpts = {
      method:  'put', 
      table: 'user',
      url:  '', 
      body: {
        username: userName,
        password: password
      }
    };
    
    this.onCallAPI(reqOpts, 
      (body) => {
        if (body.ok){
          reactCookie.save('userTokken', body.data.token, { path: '/' }); 
          this.data.user = { authTokken: body.data.token, currentApp: null };
          // change address ...
          window.location.hash = `dashboard`;
          this.trigger(this.data);
        } 
      });
  },
  
  onLogout(){
    reactCookie.remove('userTokken', { path: '/' });
    this.data.user = { authTokken: null, currentApp: null };
    this.trigger(this.data);
  },
  
  onLoadUserApps(){
    const reqOpts = {
      method:  'get', 
      table: 'app',
      url:  ''
    };
    
    this.onCallAPI(reqOpts, 
      (result) => {
        if (result.ok){
          ////console.log('your home spaces !!! ', result);
          this.data.userApps = result.data;
          this.trigger(this.data);
        }
      });
  },
  
  onCallAPI: function(opts, next){
    const { user } = this.data;
    ////console.log('user data: ', user);
    var req = request(opts.method || 'get', this.getApiUrl(opts.table, opts.url));
    {!!user.authTokken && req.set("Authorization", user.authTokken)}
    {!!user.currentApp && req.set("From", user.currentApp)}
   
    {!!opts.body &&  req.send(opts.body)}
    req.end((err, apiRes) => {
      console.warn(`API CALLED to "/${opts.url}"`, apiRes.body);
      if(!apiRes.body.ok) return this.onShowError(apiRes.body.msg);
      next(apiRes.body);
    });
  },
  
  // ERROR HANDLERS

  onShowError: function(err){
    this.data.errorMessage = err;
    clearTimeout(this._errorTimer);
    this._errorTimer = setTimeout(this.clearErrorHandler, 4000);
    this.trigger(this.data);
  },
  
  clearErrorHandler: function(){
    this.data.errorMessage = null;
    this.trigger(this.data);
  },
  
  onLoadedStore(name, store, tree){
    //console.log(name, store, tree);
    this.data.storesList[name].store = store;
    this.data.storesList[name].loaded = true;
    this.data.storesList[name].tree = tree;
    
    var counter = 0;
    var stores = Object.keys(this.data.storesList);
    stores.map(id =>
      (this.data.storesList[id].loaded && counter++ )
    );
   
    {counter === stores.length ? this.data.loaded = true : this.data.loaded = false}
    if (this.data.loaded) this.trigger(this.data);
  },
  
  onChangeTreeTo(tree){
    if (this.data.currentTree !== tree){
      this.data.currentTree = tree;
      this.trigger(this.data);
    }
  },
  
  onGoTo: function(location){
    //console.log('>>>>', location);
    
    let { currentApp,  selectedElementId, user, loaded, currentTree } = this.data;
    
     if (currentApp !== location[1] && !!loaded){
        loaded = false;
     }
     
    user.currentApp = location[1];
    this.data.currentApp = location[1];
     if (loaded){
       {location[3] !== '' && this.getPathTo(location[3])}
       this.data.currentTree = location[2];
       this.data.currentSection = location[3];
       
     }
    if (!!location[1] && !loaded && !!user.authTokken) //user.currentApp !== location[1] ||
    {
      this.data.currentTree = location[2];
      this.data.currentSection = location[3];
      this.startLoadingStores();
      //window.app.actions.loadData();
     // window.app.entryActions.loadData();
    }
    this.trigger(this.data);
  },
  
  startLoadingStores: function(){
    
    let { databaseData } = this.data;
    const reqOpts = {
      method:  'get', 
      table: 'content',
      url:  ''
    };
    
    this.onCallAPI(reqOpts, (result) => {
      // NEED MORE ATTENTION TO THIS ERROR
      if (!result.ok){
        return this.showError(result.msg);
      }
      //console.log('StoreData start loading');
      databaseData = result.data;
          
      // ==============================//
      var tree = {};
      var top = [];
          
      databaseData.map(element => {
        if (element.parent === undefined){
          //select and store all type "app" ids in to top array
          element.type === 'app' && top.push(element._id);
          tree[element._id] = this.createNode(element._id, 1, element.value, null, element.type);
        } else {
    
          // ----- Siuksliu termintorius (trina viska), naudoti tik extra atvejais ---- //
          //eee 56c9fd5d41208f03005220ff
          //entriesStore.js:120 eer 56c9fcdd41208f03005220fc
          if (element._id === "56c9fd5d41208f03005220ff") {
            //console.log('<', element.value +' ' + element._id);
            const reqOpts = {
              method: 'delete', 
              table: 'content',
              url: `entry/${element._id}`
            };
              
            this.onCallAPI(reqOpts, 
            (data) => {
            //console.log('ex', data);
            });  
          }
          //--------------------------                     ------------------------------//
          // if (element.type !== 'entry' && element.type !== 'entity') 
          tree[element._id] = this.createNode(element._id, element.parent, element.value, null, element.type, element.entry);
        }
      });
    
      tree['1'] = this.createNode('1', '1', 'Master root ..', null, 'none');
            
      // Add Edit field (depracated)
      // tree['1editField'] = this.createNode('1editField', '1editField', '', null, 'none');
      // tree['1editField'].status = 'edit';
      //console.log('pradejo krauti', tree);
      window.app.actions.loadData(top, tree, this.createNode);
      window.app.entryActions.loadData(top, tree, this.createNode);
  });
},

  createNode: function(_id, parent, value, childList, type, entry) {
    
    var entries = entries || {};
    var children = childList || {};
    
    var selected = selected || false;
   // //console.log();
    return {
      _id: _id,
      parent: parent,
      value: value,
      type: type,
      sortId: type + (2000000000000 - parseInt(_id.substring(0, 8), 16) * 1000) + _id,
      
      entry: entry || null,
      
      entries: {},
      realEntries: {},
      children: children,
      selected: true,
      status: 'show',
      draggable: (type !== 'app' && type !== 'entry' && type !== 'entity') ? true : false,
      _selected: {}
    };
  },
  
  redirectTo: function(path){
    window.location.hash = path;
    return false;
  },
  
  getPathTo: function(branch){
    this.entriesStore = window.app.entriesStore;
   
    var path = [];
    if (!!this.entriesStore){
      const { entriesTree } = this.entriesStore.data;
      var currentNode = branch;
       
      if (!!entriesTree && !!entriesTree[currentNode]){
        while (entriesTree[currentNode].parent !== 1) {
          path.unshift(currentNode);
          currentNode = entriesTree[currentNode].parent;
        } 
        path.unshift(currentNode);
      }
    }
    this.data.selectedElementPath = path;
  },
  
  onStatusToggle(e, branch){
    
    const { nodesTree } = this.data;
    nodesTree[branch._id].status = (nodesTree[branch._id].status === 'show'? 'edit' : 'show');
    //console.log(nodesTree[branch._id]);
    this.trigger(nodesTree);
  },
  
  onAddInputField(treeType, parent, type, entry){
    const { treeTypes, storesList } = this.data;
    var tree = storesList[treeType].tree;
  //console.log('<>');
    // Add Edit                                               field_id, parent,       value,          childList, type, entry
    tree[`edit${parent}`] = window.app.mainStore.createNode(`edit${parent}`, parent, '', null, 'edit', entry);
    tree[`edit${parent}`].status = 'edit';
    tree[`edit${parent}`].type = type;
    
    if (tree[tree[`edit${parent}`].entry].entries[tree[`edit${parent}`].parent] === undefined) 
          tree[tree[`edit${parent}`].entry].entries[tree[`edit${parent}`].parent] = {};
        tree[tree[`edit${parent}`].entry].entries[tree[`edit${parent}`].parent][tree[`edit${parent}`].sortId] = tree[`edit${parent}`]._id;
    
    
    
    
    // if (treeTypes[0] === treeType)  
    //   tree[parent].children[tree[`edit${parent}`].sortId] = `edit${parent}`;
      
    // if (treeTypes[1] === treeType)  
    //   tree[parent].entries[tree[`edit${parent}`].sortId] = `edit${parent}`;
  //console.log(tree, tree[`edit${parent}`]);
    this.trigger(this.data);
  },
  
  onDeselectElement(){
    const { currentApp, currentTree } = this.data;
    window.location.hash = `app/${currentApp}/${currentTree}`;
  }
});