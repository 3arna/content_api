var request = window.superagent;

window.app.actions =   Reflux.createActions(
  ['statusToggle', 'loadData', 'deleteElement']
  );

window.app.store = Reflux.createStore({
  listenables: [window.app.actions],
  
  childrenStore: null,
  mainActions: window.app.mainActions,
  
  data: {
    nodesTree: {},
    sections: [],
   // top: null,
    doomElements: [],
    structureTopEl: null
  },

  init: function() {
  },
  
  onLoadData: function(top, maintree, createNode){
    var tree = maintree;
    var sections = tree;
    
    Object.keys(sections).map(key => {
      var section = sections[key];
     // console.log(section);
      if (section.parent !== undefined){
        if (section.type !== 'entry' && section.type !== 'entity') {
          
         // console.log(section.parent);
          tree[section.parent].children[tree[section._id].sortId] = section._id;
        } else {
          tree[section.parent].draggable = false;
        }
      }
    });
    
    this.data.nodesTree = tree;
    this.data.structureTopEl = top;
    
   // console.log(tree);
    
   // console.log('STORE DATA MODIFIED', {data: this.data});
    this.mainActions.loadedStore('structure', this, tree);
    this.trigger(this.data);
  },

  saveElement: function(element, inputField){
    const { nodesTree } = this.data;
    
    inputField.value = element.value;
    inputField.status = 'show';
    this.trigger(this.data);
    
    const reqOpts = {
      method:  'post', 
      table: 'content',
      url:  'section', 
      body: {
        value:  element.value,
        type:  element.type,
        parent:  element.parent,
      }
    };
    
    this.mainActions.callAPI(reqOpts, 
      (result) => {
        var data = result.data;
      
        delete nodesTree[inputField.parent].children[inputField.sortId]; 
        delete nodesTree[inputField._id];

        nodesTree[data._id] = window.app.mainStore.createNode(data._id, data.parent, data.value, null, data.type);
        //console.log('pries', nodesTree[data.parent].children);
        nodesTree[data.parent].children[nodesTree[data._id].sortId] = data._id;
       // nodesTree[data.parent].entries[nodesTree[data._id].sortId] = data._id;
        //console.log('po', nodesTree[data.parent].children);
        this.trigger(this.data);
      });
  },
  
  changeValue: function(branch) {
    
    const { nodesTree } = this.data;
    var oldValue = nodesTree[branch._id].value;
    nodesTree[branch._id].value = branch.value;
    nodesTree[branch._id].status = 'show';
    this.trigger(this.data);
    
    const reqOpts = {
      method:  'post', 
      table: 'content',
      url:  'section', 
      body: {
        _id: branch._id,
        value: branch.value,
      }
    };
    
    this.mainActions.callAPI(reqOpts,  
      (result) => {
        if (!result.ok){
          nodesTree[branch._id].value = oldValue;
          this.trigger(this.data);
        }  
      });
  },
  
  changeParent: function(draggedObj, dropZoneId) {
    if(draggedObj._id === dropZoneId || draggedObj.parent === dropZoneId){
     return;
    }
    const { nodesTree } = this.data;
    
    // ----------- Pasalina is karto --------------//
    delete nodesTree[draggedObj.parent].children[draggedObj.sortId];
    nodesTree[dropZoneId].children[draggedObj.sortId] = draggedObj._id;
    nodesTree[draggedObj._id].parent = dropZoneId;
    this.trigger(this.data);
    //-------------------------------//
   
    const reqOpts = {
      method:  'post', 
      table: 'content',
      url:  'section', 
      body: {
        _id: draggedObj._id,
        value: draggedObj.value,
        parent: dropZoneId,
      }
    };
    
    this.mainActions.callAPI(reqOpts, 
      (result) => {
      // Missing reset after unsuccess try to write to db
      });
  },
  
  
  // SUTVARKYTA (Gerai butu padaryti bendra i mainStore) 
  onDeleteElement : function(id, update) {
    let { doomElements } = this.data;
    
    var newDoomElements = [];
    this.getAllDoomElements(newDoomElements, id);
    newDoomElements.unshift(id);
    this.trigger(this.data);
  
    this.data.doomElements = _.concat(newDoomElements, doomElements);    
    this.exterminate();
  },
  
  // Gauna visus elementus ir kartu pasalina is NodesTree
  getAllDoomElements: function(doomElements, id){
    const { nodesTree } = this.data;
    if (!!nodesTree[id] && Object.keys(nodesTree[id].children).length > 0){
      Object.keys(nodesTree[id].children).map((key) => {
        doomElements.push(nodesTree[id].children[key]);
        delete nodesTree[nodesTree[id].parent].children[nodesTree[id].sortId];
        this.getAllDoomElements(doomElements, nodesTree[id].children[key]);
      });
    }
  },
  
  exterminate: function(){
    const { doomElements, nodesTree } = this.data;
    
    if(doomElements.length){
      var ex = doomElements.pop();
      console.log(nodesTree[ex]);
      const reqOpts = {
        method: 'delete', 
        table: 'content',
        url: `section/${ex}`
      };
      
    delete nodesTree[ex];
    this.mainActions.callAPI(reqOpts,  
      (result) => {
        console.log('ex', result.data);
        this.exterminate();
      });  
    } else{
       this.mainActions.deselectElement();
    }
  },
  
  /// ??? naudojamas tik vienoje vietoje.. 
  reset: function(){
    this.trigger(this.data);
  }
  ,
});