window.app.pages.NotFound = ({page}) =>
  <app.elements.layouts.centered className="txt-center">
    <h1 className="clr-white w-100 mrg-0 pdg-tb-10px">Page not found</h1>
    <p className="clr-red">{page}</p>
  </app.elements.layouts.centered>

