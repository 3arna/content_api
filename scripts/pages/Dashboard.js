class Dashboard extends React.Component{
  
  constructor(){
    super();
    this.mainActions = window.app.mainActions;
    this.mainStore = window.app.mainStore;
    this.state = {
      currentAppId: null,
      confirmMenuOn: false,
      contextMenuOn: false,
      newAppMenuOn: false,
      userApps: null,
      pos: { x:0, y:0 }
    };
  
    this.handleContextMenu = this.handleContextMenu.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.deleteApp = this.deleteApp.bind(this);
    this.preventDefaultContextMenu = this.preventDefaultContextMenu.bind(this);
  }

  componentDidMount(){
    this.mainActions.loadUserApps();
  }
  
  componentWillReceiveProps(){
    const { userApps } = this.mainStore.data;
    this.state.userApps = userApps;
    this.forceUpdate();
  }
  
  get _getContextMenuClassNames(){ 
	  return classNames({
      'pdg-tb-2px pdg-rl-10px  fnt-sm brd-rad-2px cursor-pointer hover-clr-white list-none ': true,
      'clr-red hover-bg-red': true
    });
	}
  
  get appClassNames(){
    return classNames({
      'bg-none pdg-tb-50px pdg-rl-10px dsp-block w-100 txt-deco-none clr-white brd-blackl3': true,
      'hover-clr-white hover-bg-blackl3 cursor-pointer outline-none': true,
    });
  }
  
  get createButtonClassNames(){
    return classNames({
      'dsp-inline bg-green pdg-tb-15px pdg-rl-30px clr-white mrg-b-50px': true,
      'cursor-pointer hover-clr-green hover-bg-white': true,
    });
  }
  
  handleAddNewAppMenu(){
    this.setState({newAppMenuOn: true});
  }
  
  createApp(isConfirmed, appName){
    const { userApps } = this.state;
    //console.log(isConfirmed, appName);
    this.setState({newAppMenuOn: false});
    
    if(!isConfirmed){
      return false;
    }
      
    const reqOpts = {
      method:  'post', 
      table: 'app',
      url: '',
      body: {
        value: appName
      }
    };
  
    this.mainActions.callAPI(reqOpts,  
      (result) => {
        userApps.unshift(result.data);
        this.forceUpdate();
      });
  }
  
  handleContextMenu(e) {
    this.preventDefaultContextMenu(e, true);
    
    this.setState({
      currentAppId: e.target.getAttribute('data-appid'),
      contextMenuOn: true,
      pos: { x: e.pageX, y: e.pageY},
    });
  }
  
  preventDefaultContextMenu(e, isStopPropagation){
    isStopPropagation && e.stopPropagation();
    e && e.preventDefault() ;
    this.preventMyContextMenu(e);
    //this.state.contextMenuOn && this.setState({ contextMenuOn : false });
  }
  
  preventMyContextMenu(e){
    this.state.contextMenuOn && this.setState({ contextMenuOn : false });
  }
  
  handleDelete(e){
    this.setState({ contextMenuOn: false, confirmMenuOn: true});
  }
  
  deleteApp(isConfirmed){
    const { currentAppId } = this.state;
    if (isConfirmed) {
      const reqOpts = {
        method:  'delete', 
        table: 'app',
        url: currentAppId
      };

      this.mainActions.callAPI(reqOpts,  
        (result) => {
          if (!result.ok){

            //console.log(result);
          }  
                      this.componentDidMount();
            this.forceUpdate();
        }
      );
    }
    this.setState({confirmMenuOn: false, currentAppId: null});
  }
  
  render(){
    const { pos, userApps, contextMenuOn, confirmMenuOn, newAppMenuOn } = this.state;
    const { layouts, buttons } = window.app.elements;
    
    ////onClick={  !confirmMenuOn  && e => this.preventDefaultContextMenu(e, false)}
    return <div onClick={this.preventMyContextMenu.bind(this)} > 
        <layouts.centered >
        
        <buttons.logout onClick={this.mainActions.logout}/>
        
        <div className="w-max-800px mrg-auto pdg-rl-3" >
          <h2 className="fnt-300 w-max-400px mrg-b-50px">Dashboard huh?<br/> Lets manage some content!</h2>
          <span className={this.createButtonClassNames} onClick={this.handleAddNewAppMenu.bind(this)} >
            <i className="fa fa-plus-circle"/> Create new App
          </span>
          
          <div className="">
            {!!userApps && userApps.length > 0 && 
              userApps.map(workspace => {
                return <div key={workspace.app}  className="col-4 sm-col-12 txt-center dsp-inline pdg-5px fix-bb z-10">
                  <a href={`#app/${workspace.app}/structure`} /*onClick={e => this.mainActions.switchToUser(workspace.app)}*/
                    onContextMenu = {this.handleContextMenu}
                    className={this.appClassNames} 
                    data-appid={workspace.app}
                    data-appname={workspace.value}>
                    
                    {workspace.value}<br/>
                    <small>{workspace.app}</small>
                    
                  </a>
                </div>
              })
            }
          </div>
          {!!contextMenuOn && 
          <div className="pos-absolute bg-white w-150px brd-rad-2px pdg-2px clr-black pdg-0 mrg-0 boxshadow list-none fadeIn"
            style={{top: pos.y + 'px', left: pos.x + 'px'}} >
            <li  className={this._getContextMenuClassNames} onClick={this.handleDelete}>delete</li>
          </div>}
          {!!confirmMenuOn && <app.components.ConfirmMenu confirmed={this.deleteApp}/>}  
          {!!newAppMenuOn && <app.components.NewAppMenu confirmed={this.createApp.bind(this)}/>}
        </div>
       
    </layouts.centered>
     </div>
  }
}

window.app.pages.Dashboard = Dashboard;

