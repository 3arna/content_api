window.app.pages.Loading = () =>
  <app.elements.layouts.centered className="txt-center">
    <h1 className="top-45 pos-absolute clr-white txt-center w-100">Loading ...</h1>
  </app.elements.layouts.centered>

