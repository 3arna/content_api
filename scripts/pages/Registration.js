class Registration extends React.Component{
  
  constructor(){
    super();
    this.mainActions = window.app.mainActions;
  }
  
  handleSubmit(e){
    e.preventDefault();
    if (e.target.password.value !== e.target.confirmPassword.value){
      this.mainActions.showError('Passwords do not match !!!');
    } else {
      this.mainActions.register(e.target.username.value, e.target.password.value);
    }
  }
  
  get buttonClassNames(){
    return classNames({
      'dsp-inline brd-0 bg-green outline-none pdg-tb-10px pdg-rl-30px': true,
      'hover-clr-green hover-bg-white clr-inherit txt-deco-none': true,
    })
  }
  
  get loginButtonClassNames(){
    return classNames({
      'pos-fixed top-0 right-0 txt-deco-none cursor-pointer': true,
      'brd-l-blackl3 brd-b-blackl3 bg-black pdg-20px clr-white hover-bg-greenl1': true,
    });
  }
  
  render(){
    
    const { isLoggedIn } = this.props;
  
    return <app.elements.layouts.centered errorMessage={app.mainStore.data.errorMessage}>
      <a href={`#login`} className={this.loginButtonClassNames}>Login</a>
      
      <div className="mrg-auto w-max-400px txt-center">
        {isLoggedIn && 
          <div>
            <h2 className="fnt-300 mrg-0 mrg-b-30px">
              Go back.<br/>You are already in. 
            </h2>
            <a className={this.buttonClassNames} href="#dashboard">dashboard</a> 
          </div>}
        
        {!isLoggedIn && <form onSubmit={this.handleSubmit.bind(this)} className="txt-right fnt-lg">
          <input
            className="clr-inherit pdg-tb-15px w-100 dsp-block bg-none brd-0 brd-b-blackl3 outline-none mrg-b-20px" 
            placeholder="User name" 
            type="text" 
            name="username" 
            onChange={this.handleUserNameInput} />
          
          <input 
            className="clr-inherit pdg-tb-15px w-100 dsp-block bg-none brd-0 brd-b-blackl3 outline-none mrg-b-20px"
            placeholder="Your password" 
            type="password" 
            name="password" 
            onChange={this.handlePasswordInput} />
            
          <input 
            className="clr-inherit pdg-tb-15px w-100 dsp-block bg-none brd-0 brd-b-blackl3 outline-none mrg-b-20px"
            placeholder="Confirm password" 
            type="password" 
            name="confirmPassword" 
            onChange={this.handleConfirmPasswordInput} />
            
          <input 
            className={this.buttonClassNames} 
            type="submit" 
            value="Register"/>

        </form>}
      </div>
    </app.elements.layouts.centered>
  }
}

window.app.pages.Registration = Registration;

