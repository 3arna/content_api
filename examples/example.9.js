var request = window.superagent;

var store = Reflux.createStore({
  data: {people: []},
  
 init: function (){

		request.get("https://wdpwp.com/api/content/tree").
		end((err, apiRes) => {
		  this.data.people = JSON.parse(apiRes.text);
		  this.trigger(this.data);
		 });

  },

  getInitialState() {
    return this.data;
  }
});




var NODE_VALUE = '+';

var NewValueForm = React.createClass({
  getInitialState: function() {
    return {newValue: ''};
  },
  handleNewValueChange: function(e) {
    this.setState({ newValue: e.target.value});
  },  
  handleSubmit: function(e) {
    e.preventDefault();
    var newValue = this.state.newValue.trim();
    this.props.submitLabel(this.props.id, newValue);
    this.setState({newValue: ''});
  },
  render: function() {
    return (
      <li className="list-none">
      <form  onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="New label"
          value={this.state.newValue}
          onChange={this.handleNewValueChange}
        />
        <input type="submit" />
      </form>
    </li>
    );
  }
});



var MenuTree = React.createClass(
  {
    
    mixins: [Reflux.connect(store)],

    getInitialState: function(){
      
      return {nodesTree:
      [this.createNode(0, null, 'Menu', [1])]};
    },
    
    nextId: function() {
        this.uniqueId = this.uniqueId || 2;
        return this.uniqueId++;
    },
    
    createNode: function(id, parentId, value , children){
      
      var value = value || NODE_VALUE;
      var children = children || [];
      var selected = selected || false;
      
      return { id: id, parentId: parentId, value: value, children: children, selected: false};
    },
    
    createNewBranch: function(editedBranchId, newLabel){
      
      var tree = this.state.nodesTree;
      
      // Sukuriamas mazgas ant katik užaugintos šakos
      var newNodeId = this.nextId();
      
      var node = this.createNode(newNodeId, editedBranchId);
      tree[editedBranchId].children.push(newNodeId);
      tree[newNodeId] = node;
      
      // Sukuriamas mazgas vietoje kur katik užaugo šaka
      if (tree[editedBranchId].value == NODE_VALUE){
      var nextParent = this.nextId();
      var parentId = tree[editedBranchId].parentId;
      var newParent = this.createNode(nextParent, parentId);
      tree[parentId].children.push(nextParent);
      tree[nextParent] = newParent;
      }
      
      // Priskiria redaguojamam laukeliui nauja reiksme
      tree[editedBranchId].value = newLabel;
      tree[editedBranchId].selected = false;
      
      this.setState({nodesTree: tree});
    },
    
     toggle: function(e) {
      var tree = this.state.nodesTree;
      tree[e].selected = tree[e].selected ? false : true;
      this.setState({nodesTree: tree});
      
    },
    
    createTree : function(branchId){
      var tree = this.state.nodesTree;
      var branch = tree[branchId];
    
      if (branch.selected == false){
          // Atvaizduojama medžio šaka, be išsišakojimo
          return (<li className="list-none"><button  className="pdg-tb-2px brd-greyl2 bg-greenl3 fnt-sm clr-white brd-rad-2px brd-0 fnt-inherit dsp-block w-40 hover-bg-green trans cursor-pointer list-none" onClick={this.toggle.bind(null, branch.id)}>{branch.value}</button></li>);
      } else 
      
      // Atvaizduojamas redagavimo laukelis
      if (branch.value == NODE_VALUE){
            return (<NewValueForm  className="pdg-tb-5px brd-greyl2 bg-greenl3 fnt-sm clr-white brd-rad-2px brd-0 fnt-inherit dsp-block w-0 hover-bg-green trans cursor-pointer list-none" id={branch.id} submitLabel={this.createNewBranch} />);
      }else
      
      // Atvaizduojama medžio šaka ..
      if (branch.children.length > 0 ){
         return ([<li className="list-none"><button  className="pdg-tb-5px brd-greyl2 bg-greenl3 fnt-sm clr-white brd-rad-2px brd-0 fnt-inherit dsp-block h-3 w-10 hover-bg-green trans cursor-pointer list-none" onClick={this.toggle.bind(null, branch.id)}>{branch.value}</button></li>,
                  // .. su tos šakos vaikais.
                  <ul >{branch.children.map(this.createTree)}</ul>]);
      } 
    },
    
    setData: function(){
      this.state.people.map(
        person => {
          var newNodeId = this.nextId();
           var node = this.createNode(newNodeId, 0, person.id);
             this.state.nodesTree[newNodeId] = node;
             this.state.nodesTree[0].children.push(newNodeId);
             this.createNewBranch(newNodeId, person.id);
            // 
            // var node = this.createNode(newNodeId, 0, person.id);
            // this.state.nodesTree[newNodeId] = node;
            // this.state.nodesTree[0].children.push(newNodeId);
        });
       var newNodeId = this.nextId();
       var   node = this.createNode(newNodeId, 0);
       this.state.nodesTree[newNodeId] = node;
       this.state.nodesTree[0].children.push(newNodeId);
      this.createNewBranch(newNodeId, NODE_VALUE);
    },

    render: function() {
      if (this.state.people.data != undefined){
        console.log(this.state.people.data.map(person=>{ return person._id}));
      } 
       
      var menuRoot  = this.createTree(0);
      return (<ul className="list-none">{menuRoot}</ul>);
     }
  });
//ReactDOM.render(<App />, document.getElementById('example'));
 ReactDOM.render(
  
   <MenuTree />,
   document.getElementById('content')
 );
