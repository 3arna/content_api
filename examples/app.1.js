var NODE_VALUE = '+';

var MenuTree = React.createClass(
  {
    getInitialState: function(){
      return {nodesTree:
      [this.createNode(0, null, 'Menu', [1]),
       this.createNode(1, 0)]};
    },
    
    nextId: function() {
        this.uniqueId = this.uniqueId || 2;
        return this.uniqueId++;
    },
    
    createNode: function(id, parentId, value , children){
      
      var value = value || NODE_VALUE;
      var children = children || [];
      var selected = selected || false;
      
      return { id: id, parentId: parentId, value: value, children: children, selected: false};
    },
    
    createNewBranch: function(editedBranchId, newLabel){
      
      // Priskiria redaguojamam laukeliui nauja reiksme
      var tree = this.state.nodesTree;
      tree[editedBranchId].value = newLabel;
      tree[editedBranchId].selected = false;
      
      // Sukuriamas mazgas ant katik užaugintos šakos
      var newNodeId = this.nextId();
      
      var node = this.createNode(newNodeId, editedBranchId);
      tree[editedBranchId].children.push(newNodeId);
      tree[newNodeId] = node;
      
      // Sukuriamas mazgas vietoje kur katik užaugo šaka
      var nextParent = this.nextId();
      var parentId = tree[editedBranchId].parentId;
      var newParent = this.createNode(nextParent, parentId);
      tree[parentId].children.push(nextParent);
      tree[nextParent] = newParent;
      
      this.setState({nodesTree: tree});
    },
    
     toggle: function(e) {
      var tree = this.state.nodesTree;
      tree[e].selected = tree[e].selected ? false : true;
      this.setState({nodesTree: tree});
      
    },
    
    createTree : function(branchId){
      var tree = this.state.nodesTree;
      var branch = tree[branchId];
    
      if (branch.selected == false){
          // Atvaizduojama medžio šaka, be išsišakojimo
          return (<li className="list-none"><button  className="pdg-tb-px brd-greyl2 bg-greenl3 fnt-sm clr-white brd-rad-2px brd-0 fnt-inherit dsp-block w-40 hover-bg-green trans cursor-pointer list-none" onClick={this.toggle.bind(null, branch.id)}>{branch.value}</button></li>);
      } else 
      
      // Atvaizduojamas redagavimo laukelis
      if (branch.value == NODE_VALUE){
            return (<NewValueForm  className="pdg-tb-3px brd-greyl2 bg-greenl3 fnt-sm clr-white brd-rad-2px brd-0 fnt-inherit dsp-block w-0 hover-bg-green trans cursor-pointer list-none" id={branch.id} submitLabel={this.createNewBranch} />);
      }else
      
      // Atvaizduojama medžio šaka ..
      if (branch.children.length > 0 ){
         return ([<li className="list-none"><button  className="pdg-tb-3px brd-greyl2 bg-greenl3 fnt-sm clr-white brd-rad-2px brd-0 fnt-inherit dsp-block h-3 w-10 hover-bg-green trans cursor-pointer list-none" onClick={this.toggle.bind(null, branch.id)}>{branch.value}</button></li>,
                  // .. su tos šakos vaikais.
                  <ul >{branch.children.map(this.createTree)}</ul>]);
      }
    },

    render: function() {
      var menuRoot  = this.createTree(0);
      return (<ul className="list-none">{menuRoot}</ul>);
     }
  });
  
ReactDOM.render(
  <MenuTree />,
  document.getElementById('content')
);
