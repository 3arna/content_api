var NewValueForm = React.createClass({
    getInitialState: function() {
    return {newValue: ''};
  },
  handleNewValueChange: function(e) {
    this.setState({ newValue: e.target.value});
  },  
  handleSubmit: function(e) {
    e.preventDefault();
    var newValue = this.state.newValue.trim();
    colors = _.concat(colors, newValue);
    if (!newValue) {
      return;
    }
    this.props.onNewValueSubmit({newValue: newValue});
    // TODO: send request to the server
    this.setState({newValue: ''});
  },
  render: function() {
    return (
      <form className="newValueForm" >
        <input
          type="text"
          placeholder="New value"
          value={this.state.newValue}
          onChange={this.handleNewValueChange}
        />
        <input type="submit"/>
      </form>
    );
  }
});

  var Button = React.createClass({
    localHandleClick: function(){
   
      this.props.localHandleClick(); 
    },
    render: function() {
      return (
        <li><button onClick={this.localHandleClick}>{this.props.name}</button></li>
        )
    }
  });

var RootElement = React.createClass(
  {
    getInitialState: function(){
      return {nodesTree: [{ id: 0, parentId: null, value: 'Menu', 
      vaikas: [{ id: 1, parentId: 0, value: '+', vaikas: [], selected: '+'}], selected: '+'},{ id: 1, parentId: 0, value: '+', vaikas: [], selected: '+'}], selectedNodes: [0]};
      
    },
    nextId: function() {
        this.uniqueId = this.uniqueId || 2;
        return this.uniqueId++;
    },
     addSelected: function(e) {
      var selected = this.state.selectedNodes;
      var newLeaf = { id: this.nextId(), parentId: e.id, value: 'Briedis', vaikas: []};
      var tree = this.state.nodesTree;
      tree[e].vaikas.push(newLeaf);
      tree[newLeaf.id] = newLeaf;
    
     // selected[newLeaf.id] = newLeaf;
    
      if (tree[e].vaikas.length > 0){
       
      }
    //  alert(_.uniq(selected));
    this.setState({nodesTree: tree, selected: _.uniq(selected)});
    },
        
     toggle: function(e) {
      
      var tree = this.state.nodesTree;
      if(tree[e].selected == '-'){
        tree[e].selected = '+';
      } else {
        tree[e].selected = '-';
      }
      this.setState({nodesTree: tree});
    },
    createTree1 : function(branch){
      if (branch.value == '+'){
        return (<li><NewValueForm /></li>); 
      }else
      
      if (branch.vaikas.length > 0 && branch.selected == '-'){
         return ([<Button name={branch.value}  localHandleClick={this.toggle.bind(null, branch.id)}/>,
                  <ul>{branch.vaikas.map(this.createTree1)}</ul>]);
        
      } else {
       return (<Button  name={branch.value+branch.id}  localHandleClick={this.toggle.bind(null, branch.id)}/>);
      }
    },

    render: function() {
      var tree = this.state.nodesTree;
      var topMenu = tree[0];
      var menuTree  = this.createTree1(topMenu);
     return (<ul>{menuTree}</ul>);
     }
  });
  
ReactDOM.render(
  <RootElement />,
  document.getElementById('content')
);
