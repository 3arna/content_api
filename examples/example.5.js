var NewValueForm = React.createClass({
    getInitialState: function() {
    return {newValue: ''};
  },
  handleNewValueChange: function(e) {
    this.setState({ newValue: e.target.value});
  },  
  handleSubmit: function(e) {
    e.preventDefault();
    var newValue = this.state.newValue.trim();
    colors = _.concat(colors, newValue);
    if (!newValue) {
      return;
    }
    this.props.onNewValueSubmit({newValue: newValue});
    // TODO: send request to the server
    this.setState({newValue: ''});
  },
  render: function() {
    return (
      <form className="newValueForm" >
        <input
          type="text"
          placeholder="New value"
          value={this.state.newValue}
          onChange={this.handleNewValueChange}
        />
        <input type="submit"/>
      </form>
    );
  }
});

  var Button = React.createClass({
    localHandleClick: function(){
   
      this.props.localHandleClick(); 
    },
    render: function() {
      return (
        <li><button onClick={this.localHandleClick}>{this.props.name}</button></li>
        )
    }
  });

var RootElement = React.createClass(
  {
    getInitialState: function(){
      return {nodesTree: [{ id: this.nextId(), parentId: null, value: 'Menu', vaikas: [], selected: '-'}], selectedNodes: [0]};
      
    },
    nextId: function() {
        this.uniqueId = this.uniqueId || 0;
        return this.uniqueId++;
    },
    addNewLista: function(e) {
 
     var newLeaf = { id: this.nextId(), parentId: e, value: 'Briedis', vaikas: [], selected: '-'};
     var tree = this.state.nodesTree;

     tree[e].vaikas.push(newLeaf);
      
    this.setState({nodesTree: tree});

    },
    
    addSelected: function(e) {
      var selected = this.state.selectedNodes;
      var newLeaf = { id: this.nextId(), parentId: e.id, value: 'Briedis', vaikas: []};
      var tree = this.state.nodesTree;
      tree[e].vaikas.push(newLeaf);
      tree[newLeaf.id] = newLeaf;
    
     // selected[newLeaf.id] = newLeaf;
    
      if (tree[e].vaikas.length > 0){
       

        
      }
    //  alert(_.uniq(selected));
    this.setState({nodesTree: tree, selected: _.uniq(selected)});
    },
        
    createTree1 : function(branch){

      if (branch.vaikas.length <= 0){
        return (<Button  name={branch.value}  localHandleClick={this.addSelected.bind(null, branch.id)}/>);
      } else {
        return ([<Button name={branch.value}  localHandleClick={this.addSelected.bind(null, branch.id)}/>,
                  <ul>{branch.vaikas.map(this.createTree1)}</ul>]);
      }
    },

    render: function() {
      var tree = this.state.nodesTree;
      var topMenu = tree[0];
      var menuTree  = this.createTree1(topMenu);
     return ([<ul>{menuTree}</ul>, <Ajax url='/my/api' method='post' send={{some: 'data'}}>{
  ({error, response, done}) => !done ?
    <div>loading...</div> :
    <div>loaded! {JSON.stringify(response)}</div>
}</Ajax>);
     }
  });
  
ReactDOM.render(
  <RootElement />,
  document.getElementById('content')
);
