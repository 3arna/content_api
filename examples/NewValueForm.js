var NewValueForm = React.createClass({
  getInitialState: function() {
    return {newValue: ''};
  },
  handleNewValueChange: function(e) {
    this.setState({ newValue: e.target.value});
  },  
  handleSubmit: function(e) {
    e.preventDefault();
    var newValue = this.state.newValue.trim();
    this.props.submitLabel(this.props.id, newValue);
    this.setState({newValue: ''});
  },
  render: function() {
    return (
      <li>
      <form  onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="New label"
          value={this.state.newValue}
          onChange={this.handleNewValueChange}
        />
        <input type="submit" />
      </form>
    </li>
    );
  }
});