var NewValueForm = React.createClass({
  getInitialState: function() {
    return {newValue: ''};
  },
  localHandleClick: function(e){
    
  },
  handleNewValueChange: function(e) {
    this.setState({ newValue: e.target.value});
  },  
  handleSubmit: function(e) {
   // alert('su: ' +this.state.newValue.trim());
   //this.props.createNewLabel();
    e.preventDefault();
    //e.stopPropagation();
    var newValue = this.state.newValue.trim();
    
    this.props.submitLabel(this.props.id, newValue);
    if (!newValue) {
      return;
    }
   //this.props.onNewValueSubmit({newValue: newValue});
    // TODO: send request to the server
    this.setState({newValue: ''});
  },
  render: function() {
    return (
      <li>
      <form  onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="New label"
          value={this.state.newValue}
          onChange={this.handleNewValueChange}
        />
        <input type="submit" />
      </form>
    </li>
    );
  }
});

  var Button = React.createClass({
    localHandleClick: function(){
   
      this.props.localHandleClick(); 
    },
    render: function() {
      return (
        <li><button onClick={this.localHandleClick}>{this.props.name}</button></li>
        );
    }
  });

var MenuTree = React.createClass(
  {
    getInitialState: function(){
      return {nodesTree:
      [{ id: 0, parentId: null, value: 'Menu', children: [], selected: false},
       { id: 1, parentId: 0, value: '+', children: [], selected: false}]};
      
    },
    nextId: function() {
        this.uniqueId = this.uniqueId || 2;
        return this.uniqueId++;
    },
    createNewBranch: function(editedBranchId, newLabel){
      
      // Priskiria redaguojamam laukeliui nauja reiksme
      var tree = this.state.nodesTree;
      tree[editedBranchId].value = newLabel;
      tree[editedBranchId].selected = false;
      
      // Sukuriamas mazgas ant kurio galima būtų auginti naujas šakas
      var newNodeId = this.nextId();
      var node = { id: newNodeId, parentId: editedBranchId, value: '+', children: [], selected: false}; 
      tree[editedBranchId].children[newNodeId] = node;
      tree[newNodeId] = node;
      
      //
      tree[tree[editedBranchId].parentId].children[editedBranchId] = tree[editedBranchId];
      var nextParent = this.nextId();
      var newParent = { id: nextParent, parentId: tree[editedBranchId].parentId, value: '+', children: [], selected: false}; 
      tree[tree[editedBranchId].parentId].children[nextParent] = newParent;
      tree[nextParent] = newParent;
      
      this.setState({nodesTree: tree});
    }
    
    ,
     toggle: function(e) {
      
      var tree = this.state.nodesTree;
      var selected = tree[e].selected ? false : true;
      tree[e].selected = selected;
      this.setState({nodesTree: tree});
    },
    createTree : function(branch){
    // alert(branch.value+ ' '+ branch.selected);
      if (branch.value == '+' && branch.selected == false){
        return (<Button  name={branch.value}  localHandleClick={this.toggle.bind(null, branch.id)}/>);
     
      }else if (branch.value == '+' && branch.selected == true){
            return (<NewValueForm id={branch.id} submitLabel={this.createNewBranch} />);
      }else
      
      if (branch.children.length > 0 && branch.selected == true){
         return ([<Button name={branch.value}  localHandleClick={this.toggle.bind(null, branch.id)}/>,
                  <ul>{branch.children.map(this.createTree)}</ul>]);
        
      } else {
       return (<Button  name={branch.value}  localHandleClick={this.toggle.bind(null, branch.id)}/>);
      }
    },

    render: function() {
      var tree = this.state.nodesTree;
      tree[0].children[1] = tree[1];
      var topMenu = tree[0];
      var menuTree  = this.createTree(topMenu);
     return (<ul>{menuTree}</ul>);
     }
  });
  
ReactDOM.render(
  <MenuTree />,
  document.getElementById('content')
);
