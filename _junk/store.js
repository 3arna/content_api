var request = window.superagent;

window.app.actions =   Reflux.createActions(
  ['statusToggle']
  );

window.app.store = Reflux.createStore({
  listenables: [window.app.actions],
  
  entriesStore: null,
  mainActions: window.app.mainActions,
  
  data: {
    oldParentId: null,
    nodesTree: {},
    sections: [],
    top: null,
    doomElements: [],
    structureTopElements: null
  },

  init: function() {
    console.log('MainStore', this.mainActions);
    console.log('STORE MOUNTING');
      request.get(this.getApiUrl(''))
        .set('Content-Type', 'application/xml')
        .set("Authorization", "56c1002404c4da230899971e")
        .set("From", "56b2bde3e4b0102fef2432b6")
        .end((err, apiRes) => {
          console.log('API RES', apiRes);
          // NEED MORE ATTENTION TO THIS ERROR
          if (!apiRes.body.ok || err){
            this.mainActions.showError(apiRes.body.msg || err);
          // oldVersion return this.setData({errorMessage: apiRes.body.msg || err});
        }
          this.data.sections = apiRes.body.data;
          this.setData();
           
        this.mainActions.loadedStore('store', this);
          console.log('STORE FIRST TIME LOADED AND MADE GOTO');
        // this.data.entriesStore.setData();
          this.mainActions.goTo(window.location.hash.replace(/^#\/?|\/$/g, '').split('/'));
          
        });

  },
  
  saveElement: function(element){
    const tree = this.data.nodesTree;
    
    tree['1'].value = element.value;
    tree['1'].status = 'show';
    
    const reqOpts = {
      method:  'post', 
      url:  'section', 
      body: {
        value:  element.value,
        type:  element.type,
        parent:  element.parent,
      }
    };
    
    this.apiCallHandler(reqOpts, 
      (data) => {
        this.data.oldParentId = tree['1'].parent;
        this.removeFromOldParent(tree['1']);
        tree['1'].parent = '1';
        tree['1'].status = 'edit';
      
        tree[data._id] = this.createNode(data._id, data.parent, data.value, null, data.type);
        this.setToParent(tree[data._id]);
      });
      this.trigger(this.data);
  },
  
  changeValue: function(branch) {
      
    const tree = this.data.nodesTree;
    tree[branch._id].value = branch.value;
    
    const reqOpts = {
      method: 'post', 
      url: 'section', 
      body: {
        _id: branch._id,
        value: branch.value,
      }
    };
    
    this.apiCallHandler(reqOpts, 
      (data) => {
        // if (tree[data._id].value !== data.value){
        //   tree[data._id].value = data.value;
        //   this.trigger(this.data);
        // }
      });
    this.trigger(this.data);
  },
  
  changeParent: function(draggedObj, dropZoneId) {
    if(draggedObj._id === dropZoneId || draggedObj.parent === dropZoneId){
     return;
    }
    
    const tree = this.data.nodesTree;
    this.data.oldParentId = draggedObj.parent;
    
    // ----------- Pasalina is karto --------------//
    this.data.oldParentId && this.removeFromOldParent(draggedObj);
      
    if (tree[draggedObj._id].parent !== dropZoneId){
      tree[draggedObj._id].parent = dropZoneId;
    }
        
    this.setToParent(draggedObj);
    
    //-------------------------------//
    const reqOpts = {
      method: 'post', 
      url: 'section', 
      body: {
        _id: draggedObj._id,
        value: draggedObj.value,
        parent: dropZoneId,
      }
    };
   
    this.apiCallHandler(reqOpts, 
      (data) => {
        this.data.oldParentId && this.removeFromOldParent(data);
        if (tree[data._id].parent !== data.parent){
          tree[data._id].parent = data.parent;
          this.setToParent(data);
        }
      });
      
  },
  
  deleteElement : function(id, update) {
    
    let { doomElements } = this.data;

    var newDoomElements = [];
    this.getAllElements(newDoomElements, id);
    this.trigger(this.data);
    
    this.data.doomElements = _.concat(newDoomElements, doomElements);    
     
    this.exterminate(doomElements);
  },
  
  exterminate: function(keys){
    const { doomElements } = this.data;
    if(doomElements.length){
      const reqOpts = {
        method: 'delete', 
        url: `section/${doomElements.pop()}`
      };
      this.apiCallHandler(reqOpts, (data) => {this.exterminate(doomElements)});  
    } else{
       this.trigger(this.data);
    }
  },
  
  // Gauna visus elementus ir kartu pasalina is NodesTree
  getAllElements: function(doomElements, id){
    
    const { nodesTree } = this.data;
   // if (Object.keys(nodesTree[id].children).length > 0){
      doomElements.push(id);
      Object.keys(nodesTree[id].children).map((e) => this.getAllElements(doomElements, nodesTree[id].children[e]));
      // Pasalinti is Tevu 
      this.data.oldParentId = nodesTree[id].parent;
      this.removeFromOldParent(nodesTree[id]);
      delete nodesTree[id];
  //  }
  },

  onStatusToggle(e, branch){
    
    const { nodesTree } = this.data;
    nodesTree[branch._id].status = (nodesTree[branch._id].status === 'show'? 'edit' : 'show');
    console.log(nodesTree[branch._id]);
    this.trigger(nodesTree);
  },
  
  reset: function(){
    this.trigger(this.data);
  },
  
  removeFromOldParent: function(child){
   // console.log(child);
    var tree = this.data.nodesTree;

    var time = 2000000000000 - parseInt(child._id.substring(0, 8), 16) * 1000;
    if (child._id === "1"){
      delete tree[this.data.oldParentId].children[child._id];
    } else {
      delete tree[this.data.oldParentId].children[child.type + time];
    }
  //  console.log(tree[this.data.oldParentId].children);
  },
  
  setToParent: function(branch){
    console.log('setToParent');
    var tree = this.data.nodesTree;
    var newList = {};
    // Sets updated branch to parent
    var children = tree[branch.parent].children;
    var time = 2000000000000 - parseInt(branch._id.substring(0, 8), 16) * 1000;
    children[branch.type + time] = branch._id;
  
    //Sortinimas (simple restaking all elements)
  //  console.log('before', children);
    Object.keys(children).sort().map((id) =>
      newList[id] = children[id]
    );
  //  console.log('after', newList);
    this.data.nodesTree[branch.parent].children = newList;
    this.trigger(this.data);
  },
  
  turnOnEditField: function(parentId, type){
     var tree = this.data.nodesTree;
    tree['1'].parent = parentId;
    tree['1'].type = type;//(type== 'app') ? 'section' : 'field';
    tree[parentId].children['1'] = '1';
   // console.log(Object.keys(tree));
    this.trigger(this.data);
  },
  
  // API HELPERS
  
  getApiUrl: (url) => {
    return 'https://csgobets.herokuapp.com/api/content/' + url;
  },
  
  apiCallHandler: function(opts, next){
    request(opts.method || 'get', this.getApiUrl(opts.url))
      .send(opts.body)
      .end((err, apiRes) => {
        
        console.warn(`API CALLED to "/${opts.url}"`, {body: apiRes.body});
       
        if (!apiRes.body.ok || err){
          this.mainActions.showError(apiRes.body.msg || err);
          this.init();
        } else next(apiRes.body.data);
      });
  },

  setData: function() {
   
    var tree = {};
    var top = [];
    var sections = this.data.sections;
    
    sections.map(section => {

      if (section.parent === undefined){
        
        //select and store all type "app" ids in to top array
        section.type === 'app' && top.push(section._id);
        tree[section._id] = this.createNode(section._id, 1, section.value, null, section.type);
      } else {
        tree[section._id] = this.createNode(section._id, section.parent, section.value, null, section.type, section.entry);
      }
    });
    
    sections.map(section => {
      if (section.parent != undefined){
    
          var time = 2000000000000 - parseInt(section._id.substring(0, 8), 16) * 1000;
          if (section.entry === undefined ){
          if (section.type !== 'entry')
              tree[section.parent].children[section.type + time] = section._id;
          }
      }
    });
    
    // Add Edit field
    tree['1'] = this.createNode('1', '1', 'Element name ..', null, 'none');
    tree['1'].status = 'edit';
    
    this.data.nodesTree = tree;
 
    this.data.top = top;
    this.data.structureTopElements = top;
    console.log('WTF', this.data.structureTopElements);
    console.log('STORE DATA MODIFIED', {data: this.data});
 
    this.trigger(this.data);
  },
  
  createNode: function(_id, parent, value, children, type, entry) {
  
    var children = children || {};
    var selected = selected || false;
    
    return {
      _id: _id,
      parent: parent,
      value: value,
      type: type,
      entry: entry || null,
      
      entries: {},
      children: children,
      selected: true,
      status: 'show',
      draggable: (type !== 'app' && type !== 'entry') ? true : false
    };
  }
});