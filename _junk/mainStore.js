var request = window.superagent;

window.app.mainActions =   Reflux.createActions(
  [ 'loggin', 'getUserWorkspace', 'showError', 'loadedStore', 'changeTreeTo', 'goTo']
  );

window.app.mainStore = Reflux.createStore({
  listenables: [window.app.mainActions],
  
  entriesStore: null,
  
  storesList: {
    'entriesStore': {
      loaded: false, store: null
    },
    'store': {
      loaded: false, store: null
    }
  },
  
  data: {
    userApps: [],
    
    user: { authTokken: null, 
            currentApp: null },
    treeTypes : [
      'structureTree',
      'entitiesTree'
    ],
    
    selectedElementId: null,
    loaded: false,
    errorMessage: null,
    isEntryTree: null,
    selectedElementPath: [],
    currentTree: null
  },

  init: function() {
    this.data.currentTree = this.data.treeTypes[0];
    this.entriesStore = window.app.entriesStore;
  },
  
  getApiUrl: (table, url) => {
    return `https://csgobets.herokuapp.com/api/${table}/${url}`;
  },
  
  onLoggin(userName, password){
    const reqOpts = {
      method:  'put', 
      table: 'user',
      url:  '', 
      body: {
        username: userName,
        password: password
      }
    };
    
    this.apiCallHandler(reqOpts, 
      (body) => {
        if (body.ok){
          this.data.user = { authTokken: body.data.token, currentApp: null };
          // change address ...
           window.location.hash = `dashboard`;
           this.trigger(this.data);
        }
      });
  },
  
  onGetUserWorkspace(){
    const reqOpts = {
      method:  'get', 
      table: 'app',
      url:  ''
    };
    
    this.apiCallHandler(reqOpts, 
      (result) => {
        console.log(result);
        if (result.ok){
          console.log('your home spaces !!! ', result);
          this.data.userApps = result.data;
           this.trigger(this.data);
        }
      });
  },
  
  apiCallHandler: function(opts, next){
    const { user } = this.data;
    
    var req = request(opts.method || 'get', this.getApiUrl(opts.table, opts.url));
    {!!user.authTokken && req.set("Authorization", user.authTokken)}
    
    req.send(opts.body)
    .end((err, apiRes) => {
      console.warn(`API CALLED to "/${opts.url}"`, {body: apiRes.body});
      next(apiRes.body);
    });
  },
  
  onLoadedStore(name, store){
    this.storesList[name].store = store;
    this.storesList[name].loaded = true;
    
    var counter = 0;
    var stores = Object.keys(this.storesList);
    stores.map(id =>
      (this.storesList[id].loaded && counter++ )
    );
   
    {counter === stores.length ? this.data.loaded = true : this.data.loaded = false}
    if (this.data.loaded) this.trigger(this.data);
  },
  
  // ERROR HANDLERS

  onShowError: function(err){
    console.log('Error');
    this.data.errorMessage = err;
    clearTimeout(this._errorTimer);
    this._errorTimer = setTimeout(this.clearErrorHandler, 4000);
    this.trigger(this.data);
  },
  
  clearErrorHandler: function(){
    this.data.errorMessage = null;
    this.trigger(this.data);
  },
  
  onChangeTreeTo(tree){
    if (this.data.currentTree !== tree){
      this.data.currentTree = tree;
      this.trigger(this.data);
    }
  },
  
  onGoTo: function(location){
    let { selectedElementId } = this.data;
    selectedElementId = location[1];
    this.getPathTo(selectedElementId);
    this.trigger(this.data);
  },
  
  getPathTo: function(branch){
    var path = [];
    if (this.entriesStore){
    const { entriesTree } = this.entriesStore.data;
  
    
    var currentNode = branch;
  
    if (entriesTree && entriesTree[currentNode]){
      while (entriesTree[currentNode].parent !== 1) {
        path.unshift(currentNode);
        currentNode = entriesTree[currentNode].parent;
      } 
      path.unshift(currentNode);
    }
    }
    this.data.selectedElementPath = path;
  },
  
  
});