var request = window.superagent;

window.app.entryActions =   Reflux.createActions(
  ['changeStatus', 'reset', 'saveNewEntity', 'saveEntry', 'changeValue', 'deleteElement', 'openInputField', 'setEntryId', 'selectElement']
  );

window.app.entriesStore = Reflux.createStore({
  listenables: [window.app.entryActions],
  
  mainActions: window.app.mainActions,
  
  data: {
    sections: [],
    entriesTree: {},
    top: null,
    topEntries: {},
    doomElements: [],
    entryId: null,
    selectedElements: {}
  },
  
  store:  window.app.store,
  
    // // API HELPERS
  
  getApiUrl: (url) => {
    return 'https://csgobets.herokuapp.com/api/content/' + url;
  },
  
  onSelectElement(entryId, branch){
    const { selectedElements } = this.data;
    {!!branch.selected && 
      selectedElements[`${entryId}${branch._id}`] = true; }
    {!branch.selected && 
      delete selectedElements[`${entryId}${branch._id}`]; }
      console.log(selectedElements);
  },
  
  onChangeStatus(e, branch){
    if (branch.type === 'entity'){
      const { entriesTree } = this.data;
      entriesTree[branch._id].status = (entriesTree[branch._id].status === 'show'? 'edit' : 'show');
      this.trigger(entriesTree);
    }
  },
  
  // getPathTo: function(branch){
   
  //   var path = [];
  //   var currentNode = branch;
  
  //   if (this.data.entriesTree[currentNode]){
  //     while (this.data.entriesTree[currentNode].parent !== 1) {
  //       path.unshift(currentNode);
  //       currentNode = this.data.entriesTree[currentNode].parent;
  //     } 
  //     path.unshift(currentNode);
  //   }
  //   this.data.selectedElementPath = path;
  // },

  init: function() {
    console.log('ENTRIES STORE MOUNTING');
//     request.get(this.getApiUrl(''))
//       .set("Authorization", "56c1002404c4da230899971e")
// .set("From", "56b2bde3e4b0102fef2432b6")
//       .end((err, apiRes) => {
//         // THE SAME: NEED TO CHECK !!!!
//         if(err || !apiRes.body.ok){
//           this.mainActions.showError(apiRes.body.msg || err);
//         //OLD VERSION return this.setData({errorMessage: apiRes.body.msg || err});
//         }
//         this.data.sections = apiRes.body.data;
//         this.setEntryTemplates();
//         this.mainActions.loadedStore('entriesStore', this);

//         console.log('ENTRIES STORE FIRST TIME LOADED AND MADE GOTO');
//       // this.data.entriesStore.setData();
//         //window.app.store.goTo(window.location.hash.replace(/^#\/?|\/$/g, '').split('/'));
//       //  this.trigger(this.data);
//       });
  },
  
  setEntryTemplates: function() {
   
    var tree = {};
    var top = [];
    var sections = this.data.sections;
    
    sections.map(section => {
      if (section.parent === undefined){
        
        //select and store all type "app" ids in to top array
        section.type === 'app' && top.push(section._id);
        tree[section._id] = this.createNode(section._id, 1, section.value, null, section.type);
      } else {
      
        tree[section._id] = this.createNode(section._id, section.parent, section.value, null, section.type, section.entry);
      }
    });
            
    sections.map(section => {
      if (section.parent !== undefined){
        if (section.entry === undefined ){
          if (section.type !== 'entry' && section.type !== 'entity'){
            tree[section.parent].children[tree[section._id].sortId] = section._id;
          }
        }
      }
    });
    console.log(tree);
    // Add Edit field
    //console.log('patikrinimas: ', tree, tree['edit']);
    tree['1'] = this.createNode('1', '1', 'Element name ..', null, 'none');
  
    tree['1'].status = 'edit';
    //tree['565a3a62e4b030fba33dc046'].children.push('edit');
    
    this.data.entriesTree = tree;
 
    this.data.top = top;
    
    console.log('STORE DATA MODIFIED', {data: this.data});
 
    this.trigger(this.data);
   this.setData();
  },
  
  onSetEntryId(id){
    console.log('priskyre', id);
    this.data.entryId = id;
  },
  
  onReset: function(){
    this.trigger(this.data);
  },

  setData: function() {
    
    const { top, sections, entriesTree, topEntries } = this.data;
    //var entriesTree = {};
    

       
    //var entriesTree = {};
    var tree = {};
    var mainSections = [];
    // Gets main sections 
    
    if (top ) {
      top.map((topId) => {
        // //console.log(topId);
      // tree[topId] = entriesTree[topId];
         entriesTree[topId].entries = entriesTree[topId].children;
      });
    } 
    
    console.log(entriesTree);

    
    //Sukuria visus entries/entities
      sections.map(section => {
      if (section.type === 'entry' || section.type === 'entity'){
        
         // ----- Siuksliu termintorius (trina visus entrius/enticius), naudoti tik extra atvejais ---- //
              // console.log('<', section.value +' ' + section._id);
              // const reqOpts = {
              //   method: 'delete', 
              //   url: `${section.type}/${section._id}`
              // };
              // this.apiCallHandler(reqOpts, (data) => {
              //   console.log('ex', data);
              // });  
         //--------------------------                     ------------------------------//
        
        console.log(section.value + ' ' + section._id);
        if (section.entry === null ) {
            topEntries[section._id] = section._id;
            entriesTree[section.parent].entries[entriesTree[section._id].sortId] = section._id;

        }
        //                                    _id,      parentID,         value, children,      type,       entryId
        entriesTree[section._id] = this.createNode(section._id, section.parent, section.value, null, section.type, section.entry);
        entriesTree[section._id].entries = entriesTree[section.parent].children;
      }
    });
    
    Object.keys(entriesTree).map(entryId => {
      var item = entriesTree[entryId];
        if (item.entry !== null)
        {
          entriesTree[item.parent].entries[entriesTree[item._id].sortId] = item._id;
        } else {
                  //           entriesTree[item._id].entries = entriesTree[item.parent].children;
                  // console.log(entriesTree[item.parent].children, item._id);
        }
    });
    console.log(entriesTree);
    //--------------------------------
    
    // Add Edit field
    //console.log('patikrinimas: ', tree, tree['edit']);
    entriesTree['1'] = this.createNode('1', '1', 'Element name ..', null, 'none');
  
    entriesTree['1'].status = 'edit';
    //tree['565a3a62e4b030fba33dc046'].children.push('edit');
    
    this.data.entriesTree = entriesTree;
 
    this.data.top = top;
    
    console.log('ENTRIES STORE DATA MODIFIED', {data: this.data});
    
    this.trigger(this.data);
  },
  
  createNode: function(_id, parent, value, children, type, entry) {
    var entries = entries || {};
    var children = children || [];
    var selected = selected || false;
   // console.log();
    return {
      _id: _id,
      parent: parent,
      value: value,
      type: type,
      entry: entry || null,
      sortId:  type + (2000000000000 - parseInt(_id.substring(0, 8), 16) * 1000) + _id,
      
      realEntries: {},
      entries: {},
      children: children,
      selected: true,
      status: 'show'
    };
  },
  
  onSaveEntry: function(element){
    const reqOpts = {
      method:  'post', 
      url:  'entry', 
      body: {
        parent:  element.parent,
        entry: element.entry
      }
    };
    
    this.apiCallHandler(reqOpts, 
      (newData) => {
        const { entriesTree } = this.data;
        var newElement = entriesTree[newData._id] = this.createNode(newData._id, newData.parent, newData.value, null, newData.type, newData.entry);
        entriesTree[newElement.parent].entries[newElement.sortId] = newData._id;
        newElement.entries = entriesTree[newElement.parent].children;
        this.trigger(this.data);
      });
     
  },
  
  onSaveNewEntity: function(element){
    const { entriesTree } = this.data;

      entriesTree['1'].status = 'show';
      entriesTree['1'].value = element.value;
      this.trigger(this.data);

    
    const reqOpts = {
      method:  'post', 
      url:  'entity', 
      body: {
        value:  element.value,
        type:  element.type,
        parent:  element.parent,
        entry: element.entry
      }
    };
    
    this.apiCallHandler(reqOpts, 
      (newData) => {
        console.log(newData);
        var newElement = entriesTree[newData._id] = this.createNode(newData._id, newData.parent, newData.value, null, newData.type, newData.entry);
        entriesTree[newElement.parent].entries[newElement.sortId] = newElement._id;
        entriesTree[newElement.entry].realEntries[newElement.sortId] = newElement._id;
        
        entriesTree['1'].status = 'edit';
        var parent = entriesTree['1'].parent;
        delete entriesTree[parent].entries['1'];
        entriesTree['1'].parent = '1';
        
        this.trigger(this.data);
      });
      
  },
  
  onChangeValue: function(element) {
      
    const tree = this.data.entriesTree;
    tree[element._id].value = element.value;
    
    const reqOpts = {
      method: 'post', 
      url: element.type, 
      body: {
        _id: element._id,
        parent: element.parent,
        value: element.value,
        entry: element.entry
      }
    };
    
    this.apiCallHandler(reqOpts, 
      (data) => {
        console.log('grazinti duomenys', data);
        // if (tree[data._id].value !== data.value){
        //   tree[data._id].value = data.value;
        //   this.trigger(this.data);
        // }
      });
    this.trigger(this.data);
  },
  
  apiCallHandler: function(opts, next){
    request(opts.method || 'get', this.getApiUrl(opts.url))
      .send(opts.body)
      .end((err, apiRes) => {
        
        console.warn(`API CALLED to "/${opts.url}"`, {body: apiRes.body});
       
        if (!apiRes.body.ok || err){
          this.mainActions.showError(apiRes.body.msg || err);
          this.init();
        } else next(apiRes.body.data);
      });
  },
  
  onDeleteElement : function(id, update) {
    
    let { doomElements, entriesTree } = this.data;
    console.log(entriesTree[id]);
    var newDoomElements = [];
    this.getAllElements(newDoomElements, id, id);
    newDoomElements.push(id);
    console.log('id', newDoomElements.map((id) => entriesTree[id]));
    
    this.data.doomElements = _.concat(newDoomElements, doomElements);    
     newDoomElements.map((doomId) => this.removeFromOldParent(entriesTree[doomId]));
     this.trigger(this.data);
    console.log('trinami objektai: ', this.data.doomElements);
     //this.removeFromOldParent();
    this.exterminate();
  },
  
  exterminate: function(){
    const { doomElements, entriesTree } = this.data;
    console.log('likes masyvas')
    if(doomElements.length){
    var ex = doomElements.pop();
    
      const reqOpts = {
        method: 'delete', 
        url: `${entriesTree[ex].type}/${ex}`
      };
      this.apiCallHandler(reqOpts, (data) => {
        console.log('ex', data);
        this.exterminate();
        
      });  
    } else{
       this.trigger(this.data);
    }
  },
  
  // Gauna visus elementus ir kartu pasalina is NodesTree
  getAllElements: function(doomElements, id, mainEntry){
    
    const { entriesTree } = this.data;
    
    if (entriesTree[id] && Object.keys(entriesTree[id].entries).length > 0){
      Object.keys(entriesTree[id].entries).map((key) => {
        this.getAllElements(doomElements, entriesTree[id].entries[key], mainEntry);
      });
    }
        if (Object.keys(entriesTree[mainEntry].realEntries).indexOf(entriesTree[id].sortId) > -1){
          console.log('d', entriesTree[id].value);
          doomElements.push(id);
        }
  },
  
  removeFromOldParent: function(child){
    var tree = this.data.entriesTree;
    
    if (child.type === 'entity' || child.type === 'entry') {
      console.log(`just deleted: ${child.value}`);
      delete tree[child.parent].entries[child.sortId];
    }
   // console.log('parent', tree[child.parent].entries);
  },
  
  onOpenInputField: function(parent, entry, type){
    const { entriesTree } = this.data;
    entriesTree['1'].parent = parent;
    entriesTree['1'].type = type;
    entriesTree[parent].entries["1"] = '1';
    entriesTree['1'].entry = entry;
     
    console.log(entriesTree[parent].entries);
    entriesTree[entry].realEntries["1"] = '1';
     
     this.trigger(this.data);
  },
  
  
  
});