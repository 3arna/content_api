createTree: function(branchId, sections) {

    var branch = _.find(sections, {_id: branchId});
    var branchChildren = _.filter(sections, {parent: branchId});
    branch.selected = true;
    var that = this;
    
    var sectionClassNames = classNames({
      'pdg-tb-3px mrg-tb-1px fnt-sm brd-rad-2px list-none brd-0': true,
      'dsp-inline valign-bot trans cursor-pointer fnt-inherit outline-none': true,
      'hover-bg-green hover-clr-white': true,
      'bg-greenl1 clr-white': branch.type === 'app',
      'bg-black clr-white': branch.type === 'section',
      'bg-none': branch.type === 'field',
    });
    
    var iconClassNames = classNames({
      'fa fa-fw': true,
      'fa-angle-down': branch.selected,
      'fa-angle-right': !branch.selected
    });
      // .. su tos šakos vaikais.
      
    return (
      <li key={branchId} draggable="true" onDrag={this.dragging} className="list-none">
        <button  className={sectionClassNames} onClick={this.toggle.bind(null, branch.id)}>
          {branch.type !== 'field' && 
            <i className={iconClassNames}/>}
          {branch.name}
        </button>
        
        {!!branchChildren.length && branch.selected &&
          <ul className="mrg-0 pdg-l-20px">
            {branchChildren.map( (child) => that.createTree(child._id, sections) )}
          </ul>
        }
      </li>
    );
  },